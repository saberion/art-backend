<?php

namespace App\Exports;

use App\Models\Student;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

use Illuminate\Http\Request;

class StudentExport implements FromView
{

    protected $students;

    function __construct($students) {
            $this->students = $students;
    }

    public function view(): View
    {
        return view('exports.student_list', [
            'students' => $this->students
        ]);
    }
}
