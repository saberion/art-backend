<?php

namespace App\Mail;

use App\Models\Student;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class StudentRegistered extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The student instance.
     *
     * @var Student
     */
    protected $student;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Student $student)
    {
        $this->student = $student;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return  $this->from(config('mail.from.address'), config('mail.from.name'))
                ->subject("Drawing Submission - ".$this->student->student_id)
                ->view('emails.students.registered', [ 'student' => $this->student]);
    }
}
