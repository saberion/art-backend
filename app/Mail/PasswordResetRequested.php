<?php

namespace App\Mail;

use App\Models\Student;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PasswordResetRequested extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The student instance.
     *
     * @var Student
     */
    protected $student;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Student $student)
    {
        $this->student = $student;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.students.passwordResetRequested')->with([
            'url' => url()->temporarySignedRoute(
                'students.password.reset',
                now()->addMinutes(30),
                ['id' => $this->student->id]
            )
        ]);
    }
}
