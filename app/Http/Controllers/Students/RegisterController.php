<?php

namespace App\Http\Controllers\Students;

use App\Models\Entry;
use App\Models\Student;
use App\Http\Controllers\Controller;
use App\Http\Requests\Students\RegisterRequest;
class RegisterController extends Controller
{
    /**
     * Show student's registration form.
     *
     * @return \Illuminate\View\View
     */
    public function showRegistrationForm()
    {
        if (request()->filled('type') && request('type') === 'individual') {
            return view('students.auth.individual');
        }

        if (request()->filled('type') && request('type') === 'group') {
            return view('students.auth.group');
        }

        abort(404);
    }

    /**
     * Handle student registration.
     *
     * @param  Register $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function register(Register $request)
    {
        $students = $this->verifyRegistered($request->validated()['students']);

        if ($students instanceof \Illuminate\Http\RedirectResponse) {
            return $students;
        }

        $entry = $this->createEntry();

        $this->createStudents($students, $entry->id);

        $message = "Registration successful. Your Competitor ID is {$entry->identity}. Please login.";

        return redirect()->route('root')->with(['success'   => $message]);
    }

    /**
     * Check whether the incoming entry can be attached to existing students.
     *
     * @param  array $students
     * @return array|\Illuminate\Http\RedirectResponse
     */
    public function verifyRegistered(array $students)
    {
        $existingStudents = [];

        foreach ($students as $key => $student) {
            $existingStudent = $this->getExistingStudent($student);

            if (!$existingStudent) {
                continue;
            }

            if ($existingStudent->entries->count() === 2) {
                $error = $student['name'] . ' already has 2 entries.';

                return back()->withInput()->with(['error' => $error]);
            }

            if ($existingStudent->entries->count() === 1) {
                $validity = $this->isValidEntry($existingStudent);

                if (! $validity['status']) {
                    return back()->withInput()
                        ->with(['error' => $validity['error']]);
                }

                unset($students[$key]);

                array_push($existingStudents, $existingStudent);
            } else {
                return back()->withInput()
                    ->with('error', 'Please contact system administrator.');
            }
        }

        return [
            'submittedStudents' => $students,
            'existingStudents'  => $existingStudents
        ];
    }

    /**
     * Get existing student using incoming request values.
     *
     * @param  array $student
     * @return Student
     */
    public function getExistingStudent(array $student)
    {
        return Student::withTrashed()->select(['id', 'name'])
            ->with(['entries:id,type'])
            ->where(['nic' => $student['nic']])
            ->orWhere(['email' => $student['email']])
            ->first();
    }

    /**
     *  Check if the specified student can create an entry of incoming type.
     *
     * @param  Student $student
     * @return array
     */
    public function isValidEntry(Student $student)
    {
        $incomingStudents = count(request('students'));

        $existingEntry = $student->entries->first()->type;

        if ($incomingStudents === 1 && $existingEntry === 'individual') {
            return [
                'status'=> false,
                'error' => $student->name . ' already has an individual entry.'
            ];
        }

        if ($incomingStudents === 3 && $existingEntry === 'group') {
            return [
                'status'=> false,
                'error' => $student->name . ' already has a group entry.'
            ];
        }

        return ['status'=> true];
    }

    /**
     * Create an entry.
     *
     * @return Entry $entry
     */
    public function createEntry()
    {
        $type = count(request('students')) > 1 ? 'group' : 'individual';

        $entry = Entry::create(['identity' => 'PCCSDC', 'type' => $type]);

        $entry->identity .= $entry->id;

        $entry->save();

        return $entry;
    }

    /**
     * Create students.
     *
     * @param  array $students
     * @param  int $entryId
     * @return void
     */
    public function createStudents(array $students, int $entryId)
    {
        $this->setEntryToRegistered($students, $entryId);

        if (! empty($students['submittedStudents'])) {
            foreach ($students['submittedStudents'] as $student) {
                $student['password'] = request('students.0.password');
                $student['email_verified_at'] = now();

                $student = Student::create($student);

                $student->entries()->attach($entryId);
            }
        }
    }

    /**
     * Attach newly created entry to existing students.
     *
     * @param  array $students
     * @param  int $entryId
     * @return void
     */
    public function setEntryToRegistered(array $students, int $entryId)
    {
        if (! empty($students['existingStudents'])) {
            foreach ($students['existingStudents'] as $student) {
                $student->entries()->attach($entryId);
            }
        }
    }
}
