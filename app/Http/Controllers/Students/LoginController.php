<?php

namespace App\Http\Controllers\Students;

use App\Models\Entry;
use App\Models\Student;
use App\Http\Controllers\Controller;
use App\Http\Requests\Students\LogIn;

class LoginController extends Controller
{
    /**
     * Show the student's login form.
     *
     * @return \Illuminate\View\View
     */
    public function showLoginForm($id = null)
    {
        if (! $id) {
            return view('students.auth.login');
        }

        $student = Student::with(['entries:id,identity'])
            ->findOrFail($id, ['id']);

        return view('students.auth.login', compact('student'));
    }

    /**
     * Handle student login.
     *
     * @param  LogIn $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function login(LogIn $request)
    {
        $validated = $request->validated();

        $entry = Entry::where(['identity' => $validated['username']])->first();

        if (! $entry) {
            $error = 'No entry found by the id of ' . $validated['username'] . '.';

            return back()->withInput()->with(['error' => $error]);
        }

        if ($this->isAuthenticatable($validated)) {
            return $this->isEmailVerified($entry);
        }

        return back()->withInput()->with([
            'error' => 'Email/password is incorrect.'
        ]);
    }

    /**
     * Attempt to authenticate a student using given credentials.
     *
     * @param  array $validated
     * @return bool
     */
    public function isAuthenticatable(array $validated)
    {
        return auth()->guard('student')->attempt([
            'email'     => $validated['email'],
            'password'  => $validated['password']
        ]);
    }

    /**
     * Check whether the authenticated student has verified email.
     *
     * @param  Entry $entry
     * @return \Illuminate\Http\RedirectResponse
     */
    public function isEmailVerified($entry)
    {
        $student = auth()->guard('student')->user();

        if (! $student->email_verified_at) {
            $this->logout(false);

            return back()->withInput()
                ->with(['error' => 'Please verify your email.']);
        }

        session(['entry' => $entry]);

        return redirect()->route('students.profile.index');
    }

    /**
     * Log the student out of the application.
     *
     * @param  bool $return
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout($return = true)
    {
        auth()->guard('student')->logout();

        request()->session()->invalidate();

        request()->session()->regenerateToken();

        if ($return) {
            return redirect()->route('root');
        }
    }
}
