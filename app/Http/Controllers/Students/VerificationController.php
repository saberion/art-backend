<?php

namespace App\Http\Controllers\Students;

use App\Models\Student;
use App\Http\Controllers\Controller;

class VerificationController extends Controller
{
    /**
     * Mark the specified user's email address as verified.
     *
     * @param  int  $id
     */
    public function verify($id)
    {
        $student = Student::with(['entries:id,identity'])->findOrFail($id, ['id', 'email']);

        $student->email_verified_at = now();

        $student->save();

        return view('students.auth.login', compact('student'));
    }
}
