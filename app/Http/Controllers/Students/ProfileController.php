<?php

namespace App\Http\Controllers\Students;

use App\Models\Student;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Students\UpdateProfile;

class ProfileController extends Controller
{
    /**
     * Show individual/group profile.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        if (! session()->has('entry')) {
            abort(404);
        }

        $entry = session('entry');

        $entry->load(['students']);

        return view('students.profile.index', compact('entry'));
    }

    /**
     * Edit individual/group profile.
     *
     * @return \Illuminate\View\View
     */
    public function edit()
    {
        if (! session()->has('entry')) {
            abort(404);
        }

        $entry = session('entry');

        $entry->load(['students']);

        return view('students.profile.edit', compact('entry'));
    }

    /**
     * Update individual/group profile.
     *
     * @return \Illuminate\View\View
     */
    public function update(UpdateProfile $request)
    {
        foreach($request->validated()['students'] as $id => $values) {
            if (! empty($values['avatar'])) {
                $values = $this->updateAvatar($id, $values);
            }

            Student::where(['id' => $id])->update($values);
        }

        return redirect()->route('students.profile.index')
            ->with(['success' => 'Profile updated.']);
    }

    /**
     * Update specified student's avatar.
     *
     * @param  int $id
     * @param  array $values
     * @return array
     */
    public function updateAvatar(int $id, array $values)
    {
        $student = Student::findOrFail($id, ['avatar']);

        Storage::delete([$student->avatar]);

        $values['avatar'] = $values['avatar']
            ->store('images/students', ['disk' => 'public']);

        return $values;
    }
}
