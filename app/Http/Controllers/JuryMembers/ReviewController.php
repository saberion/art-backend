<?php

namespace App\Http\Controllers\JuryMembers;

use App\Models\Submission;
use App\Http\Controllers\Controller;
use App\Http\Requests\JuryMembers\Review;

class ReviewController extends Controller
{
    /**
     * Show the form for creating a new review.
     *
     * @return \Illuminate\View\View
     */
    public function create($id)
    {
        $submission = Submission::with(['entry:id,identity'])
            ->findOrFail($id, ['id', 'entry_id']);

        if ($submission->review()->count()) {
            return redirect()->route(
                'juryMembers.dashboard.index',
                [
                    'competition'   => $submission->competition,
                    'page'          => request('page', 1)
                ]
            )->with(['error' => 'Already reviewed']);
        }

        return view('juryMembers.review.create', compact('id', 'submission'));
    }

    /**
     * Store review.
     *
     * @param  mixed $id
     * @param  Review $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store($id, Review $request)
    {
        $submission = Submission::findOrFail($id, ['id', 'competition']);

        if ($submission->review()->count()) {
            return redirect()->route(
                'juryMembers.dashboard.index',
                [
                    'competition'   => $submission->competition,
                    'page'          => request('page', 1)
                ]
            )->with(['error' => 'Already reviewed']);
        }

        $review = auth()->guard('juryMember')->user()->reviews()
            ->make($request->validated());

        $submission->reviews()->save($review);

        return redirect()->route(
            'juryMembers.dashboard.index',
            [
                'competition'   => $submission->competition,
                'page'          => request('page', 1)
            ]
        )->with(['success' => 'Review saved']);
    }
}
