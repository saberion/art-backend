<?php

namespace App\Http\Controllers\JuryMembers;

use App\Models\Submission;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    /**
     * Show jury member dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        if (! request()->filled('competition')) {
            abort(404);
        }

        $competition = request('competition');

        $submissions = Submission::competition($competition)
            ->doesntHave('review')
            ->with(['entry:id,identity'])
            ->with(['review:id,submission_id,points,comments'])
            ->paginate(15);

        return view('juryMembers.dashboard.index', compact(
            'submissions', 'competition'
        ));
    }
}
