<?php

namespace App\Http\Controllers\JuryMembers;

use App\Http\Controllers\Controller;
use App\Http\Requests\JuryMembers\LogIn;

class LoginController extends Controller
{
    /**
     * Show the jury member's login form.
     *
     * @return \Illuminate\View\View
     */
    public function showLoginForm()
    {
        return view('juryMembers.auth.login');
    }

    /**
     * Handle jury member login.
     *
     * @param  LogIn $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function login(LogIn $request)
    {
        $validated = $request->validated();

        $authenticated = auth()->guard('juryMember')->attempt([
            'email'     => $validated['email'],
            'password'  => $validated['password']
        ]);

        if (! $authenticated) {
            return back()->withInput()->with([
                'error' => 'Email/password is incorrect.'
            ]);
        }

        return redirect()
            ->route('juryMembers.dashboard.index', ['competition' => 1]);
    }

    /**
     * Log the jury member out of the application.
     *
     * @param  bool $return
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout($return = true)
    {
        auth()->guard('juryMember')->logout();

        request()->session()->invalidate();

        request()->session()->regenerateToken();

        if ($return) {
            return redirect()->route('root');
        }
    }
}
