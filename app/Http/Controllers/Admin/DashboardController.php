<?php

namespace App\Http\Controllers\Admin;

use App\Models\Student;
use App\Http\Controllers\Controller;
use App\Exports\StudentExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Intervention\Image\Facades\Image;
use File;
use Artisan;
use ZipArchive;

class DashboardController extends Controller
{


    function __construct(Student $students)
    {
        $this->students = New Student;
    }

    /**
     * Show admin dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $filters = array_filter($request->all());
        $students = $this->students;

        if(isset($filters['age_group'])){
            $students = $students->where('age_group' , $request['age_group']);
        }

        if(isset($filters['stage'])){
            $stage = $request['stage'] == 'Initial' ? null : $request['stage'];
            $students = $students->where('stage' , $stage);
        }

        if(isset($filters['keyword'])){
            $students = $students->where('student_id' , 'LIKE' , "%{$request['keyword']}%");
            $students = $students->orWhere('name' , 'LIKE' , "%{$request['keyword']}%");
            $students = $students->orWhere('email' , 'LIKE' , "%{$request['keyword']}%");
        }

        if(in_array(auth()->user()->email, config('properties.admins')) && isset($filters['trash_only'])){
            $students = $students->onlyTrashed();
        }


        if(isset($filters['reset']) &&  $request['reset'] == '1'){
            return redirect()->route('admin.dashboard.index');
        }

        if (isset($filters['action'])) {

            if($request['action'] == 'export'){
                $export_student = $students->orderBy('created_at', 'DESC')->get([
                    'student_id' ,
                    'age_group' ,
                    'stage',
                    'name' ,
                    'date_of_birth' ,
                    'address',
                    'school',
                    'parent_name',
                    'contact',
                    'email',
                    'created_at'
                ]);
                return Excel::download(new StudentExport($export_student), 'Students.xlsx', \Maatwebsite\Excel\Excel::XLSX);
            }

            if($request['action'] == 'download_images'){

                if (!isset($filters['age_group'])) {
                    return redirect()->back()->with(['error' => 'Please select a age group for export images']);
                }

                $master_age_groups = Config('properties.age_groups');
                $zipname = 'Age-'.$master_age_groups[$filters['age_group']]['name'].'-drawings.zip';
                $zip = new ZipArchive;
                $target_file = storage_path('app/drawings/'.$master_age_groups[$filters['age_group']]['name']);

                if (File::exists($target_file) && $zip->open(public_path($zipname), ZipArchive::CREATE) === TRUE) {

                    try {

                        $files = File::files($target_file);
                        foreach ($files as $key => $value) {
                            $relativeNameInZipFile = basename($value);
                            $zip->addFile($value, $relativeNameInZipFile);
                        }
                        $zip->close();

                    return response()->download(public_path($zipname))->deleteFileAfterSend(true);
                    } catch (\Throwable $th) {
                       return redirect()->back()->with(['error' => 'Drawing download failed due to critical error. error type: '.$th->getMessage()]);
                    }
                }

            }

        }


        $students = $students->orderBy('created_at', 'DESC')->paginate(100);

        return view('admin.dashboard.index', [
            'students' => $students
        ]);
    }


    public function export()
    {
        return Excel::download(new StudentExport, 'Students.xlsx');
    }


    public function edit($id)
    {
        return view('admin.entries.details', [
            'student' =>  $this->students->findOrfail($id)
        ]);
    }


    public function RotateImage(Request $request)
    {
        $student = $this->students->findOrfail($request->id);
        if (file_exists(public_path($student->file_path))) {

            $file_info   = pathinfo(public_path($student->file_path));
            $allowed_mie_types = ['jpeg', 'jpg', 'png', 'gif'];

            if ($file_info && in_array($file_info['extension'], $allowed_mie_types) ) {

                $make_img    = Image::make(public_path($student->file_path));
                $make_img->rotate($request->position);
                $make_img->save($student->file_path, 90);
                Artisan::call('cache:clear');
                return ['success' => true];

            }

        }
    }

    public function WatermarkImage(Request $request)
    {
        $student = $this->students->findOrfail($request->id);
        if (file_exists(public_path($student->file_path))) {

            $file_info   = pathinfo(public_path($student->file_path));
            $allowed_mie_types = ['jpeg', 'jpg', 'png', 'gif'];

            if ($file_info && in_array($file_info['extension'], $allowed_mie_types) ) {

                $make_img    = Image::make(public_path($student->file_path));
                $make_img->text('Arunalu Siththam '.date('Y'), $make_img->width() - 260 , $make_img->height()-30, function($font) {
                    $font->file(public_path('assets/fonts/nunito-semibold-webfont.woff'));
                    $font->size(22);
                    $font->color('#ffff');
                    $font->align('left');
                    $font->valign('top');
                });

                $make_img->save($student->file_path, 90);
                Artisan::call('cache:clear');
                return ['success' => true];

            }

        }
    }



    /**
     * Delete entry & related students, submissions, reviews.
     *
     * @param  mixed $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function Trash($id)
    {
        $student = Student::findOrFail($id, ['id']);
        $student->delete();
        return back()->with(['success' => 'Participant trashed']);
    }

    public function Restore($id)
    {
        $student = Student::withTrashed()->findOrFail($id, ['id']);
        $student->restore();
        return back()->with(['success' => 'Participant Restored']);
    }

    public function Drawing($st_id)
    {

            if($student = Student::where(['student_id' => $st_id])->withTrashed()->first()){

            if (file_exists(public_path($student->file_path))) {

                $path = public_path($student->file_path);
                $file = File::get($path);
                $type = File::mimeType($path);

                $response = Response::make($file, 200);

                $response->header("Content-Type", $type);
                return $response;

            }else{
                abort(404);
            }

        }else{
            abort(404);
        }

    }


    public function UpdateStage($st_id)
    {

        if(empty($st_id)){
            abort(404);
        }

        if($student = Student::where(['id' => $st_id])->first()){
            $stages = config('properties.stages');
            $current_stage = $student->stage !== null ? $student->stage : 0;

            $max_stage = array_key_last($stages);
            $next_stage = $current_stage < $max_stage ? $current_stage + 1 : $current_stage;
            $student->stage = $next_stage;

            if($student->save()){
                return redirect()->back()->with(['success' => 'Competitor moved to the next level']);
            }
        }else{
            abort(404);
        }

    }

    function resubmit(){
        if (auth()->user()->email == 'admin@saberion.com' || auth()->user()->email == 'gayan_piumal@arunalusiththam.lk'){
            return view('frontend.index');
         }

         abort(404);
    }


}
