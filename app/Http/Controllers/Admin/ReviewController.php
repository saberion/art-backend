<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class ReviewController extends Controller
{
    /**
     * Show all scores.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $results = $this->getResults();

        return view('admin.reviews.index', compact('results'));
    }

    /**
     * Get paginated reviews with selected joined columns.
     *
     * @return Illuminate\Pagination\LengthAwarePaginator
     */
    public function getResults()
    {
        if (request()->filled('competition')) {
            return DB::table('reviews')
                ->join('jury_members', 'reviews.jury_member_id', '=', 'jury_members.id')
                ->join('submissions', 'reviews.submission_id', '=', 'submissions.id')
                ->join('entries', 'submissions.entry_id', '=', 'entries.id')
                ->select(['identity', 'name', 'points', 'proposal', 'design', 'comments'])
                ->where(['competition' => request('competition')])
                ->paginate(15);
        }

        return DB::table('reviews')
            ->join('jury_members', 'reviews.jury_member_id', '=', 'jury_members.id')
            ->join('submissions', 'reviews.submission_id', '=', 'submissions.id')
            ->join('entries', 'submissions.entry_id', '=', 'entries.id')
            ->select(['identity', 'name', 'points', 'proposal', 'design', 'comments'])
            ->paginate(15);
    }
}
