<?php

namespace App\Http\Controllers\Admin;

use App\Models\Entry;
use App\Http\Controllers\Controller;

class EntryController extends Controller
{
    public function students($id)
    {
        $entry = Entry::with(['students'])->findOrFail($id, ['id']);

        return view('admin.entries.students', compact('entry'));
    }
}
