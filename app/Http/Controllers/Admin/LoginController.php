<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\LogIn;

class LoginController extends Controller
{
    /**
     * Show the admin's login form.
     *
     * @return \Illuminate\View\View
     */
    public function showLoginForm()
    {
        return view('admin.auth.login');
    }

    /**
     * Handle admin login.
     *
     * @param  LogIn $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function login(LogIn $request)
    {
        $validated = $request->validated();

        $authenticated = auth()->guard('admin')->attempt([
            'email'     => $validated['email'],
            'password'  => $validated['password']
        ]);

        if (! $authenticated) {
            return back()->withInput()->with([
                'error' => 'Email/password is incorrect.'
            ]);
        }

        return redirect()->route('admin.dashboard.index');
    }

    /**
     * Log the admin out of the application.
     *
     * @param  bool $return
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout($return = true)
    {
        auth()->guard('admin')->logout();

        request()->session()->invalidate();

        request()->session()->regenerateToken();

        if ($return) {
            return redirect()->route('admin.login');
        }
    }
}
