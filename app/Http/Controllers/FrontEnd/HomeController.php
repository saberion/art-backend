<?php

namespace App\Http\Controllers\FrontEnd;

use App\Http\Controllers\Controller;
use App\Models\Student;
use Symfony\Component\HttpFoundation\Request;
use Illuminate\Support\LazyCollection;
use Intervention\Image\Facades\Image;

class HomeController extends Controller
{
    /**
     * Show homepage.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $students = Student::orderBy('created_at', 'asc');
        $student_count = Student::count();

        if ($request->has('age_group') && $request->age_group !== null) {
            $students->where('age_group', $request->age_group);
        }

        if ($request->has('find') && $request->find !== null) {
            $students->where('name', 'like', '%' . $request->find . '%')
            ->orWhere('contact', $request->find)
            ->orWhere('email', $request->find)
            ->orWhere('student_id', $request->find);
        }

        $students = $students->skip($request->offset ?? 0)->take($request->take ?? 12)->get();
        $age_groups = config('properties.age_groups', []);

        if ($request->ajax() && $students) {

            $response['data'] = $students->toArray();
            $response['offset'] = $request->offset;
            $response['take'] = $request->take;

            return $response;
        }

        return view('frontend.reuslts', compact('students', 'age_groups', 'student_count'));
    }

    public function view($student_id)
    {

        $response['student'] = Student::findOrfail($student_id);
        $response['success'] = true;
        $response['view'] =  view('frontend.model', ['student' => $response['student']])->render();

        return $response;
    }


    public function showGenerator()
    {
        return view('frontend.image_process');
    }


    public function generateWatermarkImage(Request $request)
    {
        $students = Student::skip($request->offset)->take($request->take)->get();
        $count = Student::count();

        if ($request->take >= $count) {
            return ['complete' => true];
        }

        $x = 0;

        foreach ($students as $key => $student) {

                if (is_readable(public_path($student->file_path))) {
                    $file_info   = pathinfo(public_path($student->file_path));
                    $allowed_mie_types = ['jpeg', 'jpg', 'png', 'gif'];

                if ($file_info && in_array($file_info['extension'], $allowed_mie_types) ) {
                    $make_img    = Image::make(public_path($student->file_path));
                    $full_path   = public_path($student->file_path);

                    if (!file_exists($full_path)) {
                        $make_img->text('Arunalu Siththam '.date('Y'), $make_img->width() - 260 , $make_img->height()-30, function($font) {
                            $font->file(public_path('assets/fonts/nunito-semibold-webfont.woff'));
                            $font->size(22);
                            $font->color('#ffff');
                            $font->align('left');
                            $font->valign('top');
                        });
                        $make_img->save($full_path, 90);
                    }
                }
                }




        }

        $offset = $request->take;
        $take   = $request->take + 50;


        if($take > $count){
            $take = $count - $request->take + $offset;
        }


        $next_record_details = ['offset' => $offset , 'take' => $take];



        return  $next_record_details;

        return ['complete' => true];

    }

}
