<?php

namespace App\Http\Controllers\FrontEnd;

use App\Models\Result;
use App\Http\Controllers\Controller;
use App\Http\Requests\Students\RegisterRequest;
use Illuminate\Support\Facades\Mail;
use App\Mail\StudentRegistered;
use App\Models\Student;
use Carbon;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Response;

class StudentController extends Controller
{
    /**
     * Show competition one page
     *
     * @return \Illuminate\View\View
     */


    public function store(RegisterRequest $request)
    {
        $now = Carbon\Carbon::now();
        $data = $request->all();
        $status = true;
        $data['age_group'] = null;
        $data['file_path'] = null;
        $age_groups = config('properties.age_groups');

        $student_id = 'CAS'.date('Y').'-'.$now->format('mdhis');
        $age = Carbon\Carbon::parse($request->date_of_birth)->age;

        foreach ($age_groups as $key => $group) {
            $ages = range($group['from'], $group['to']);
            in_array($age , $ages) ? $data['age_group']  = $key : null;
        }

        if($data['age_group'] == null){
            $status = false;
            $response['errors']['date_of_birth'] =  [0 =>'The date of birth must be in range of 4 - 16 years old'];
            return response()->json($response, 422);
        }

        if($request->has('drawing') && $request->file('drawing') !== null){
            $data['file_path'] = $request->file('drawing')->storeAs(
                'drawings/'.$age_groups[$data['age_group']]['name'], $student_id.'.'.$request->file('drawing')->extension()
            );
        }

        if($data['file_path'] == null){
            $status = false;
            $response['errors']['drawing'] =  [0 =>'Please upload your drawing'];
            return response()->json($response, 422);
        }

        $data['student_id'] = $student_id;

        if($status == true){
            if($student = Student::create($data)){
                Mail::to($student->email)->send(new StudentRegistered($student));
                $request->session()->flash('success' , 'Your drawing has been submitted');
            }
        }else{
            $request->session()->flash('error' , 'Oops. Something went wrong. try again!');
        }

        return $status;

    }



}
