<?php

namespace App\Http\Requests\Students;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'           =>  'required|string|max:255',
            'date_of_birth'  =>  'required|date',
            'school'         =>  'required|string|max:255',
            'parent_name'    =>  'required|string|max:255',
            'contact'        =>  'nullable|string|min:10|max:13',
            'email'          =>  'required|string|email|max:255',
            'drawing'        =>  'required',
            'drawing.*'      =>  'image|mimes:jpeg,png,jpg|max:2048',
            'terms'          =>  'accepted',
            'address'        =>  'required|string',
        ];
    }

    public function messages()
    {
        return [
            'terms.accepted'   =>  'Please agree with the terms &amp; conditions',
        ];
    }


}
