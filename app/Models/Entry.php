<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Entry extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['identity', 'type'];

    /**
     * The students that belong to the entry.
     */
    public function students()
    {
        return $this->belongsToMany('App\Models\Student');
    }

    /**
     * Get the submissions of the entry.
     */
    public function submissions()
    {
        return $this->hasMany('App\Models\Submission');
    }

    /**
     * Get the submission of the specified competition.
     */
    public function competition($number)
    {
        return $this->hasOne('App\Models\Submission')->competition($number);
    }

    /**
     * Get the submission of competition one.
     */
    public function competitionOne()
    {
        return $this->hasOne('App\Models\Submission')->competition(1);
    }

    /**
     * Get the submission of competition two.
     */
    public function competitionTwo()
    {
        return $this->hasOne('App\Models\Submission')->competition(2);
    }
}
