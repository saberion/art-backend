<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Review extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'jury_member_id',
        'submission_id',
        'points',
        'comments'
    ];

    /**
     * Get the submission that owns the review.
     */
    public function juryMember()
    {
        return $this->belongsTo('App\Models\JuryMember');
    }

    /**
     * Get the submission that owns the review.
     */
    public function submission()
    {
        return $this->belongsTo('App\Models\Submission');
    }

    /**
     * Scope a query to only include the review of the specified
     * jury member.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAuthor($query, $id)
    {
        return $query->where(['jury_member_id' => $id]);
    }
}
