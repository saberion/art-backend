<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User;
use Illuminate\Database\Eloquent\SoftDeletes;
use Image;

class Student extends User
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'student_id',
        'name',
        'date_of_birth',
        'parent_name',
        'contact',
        'stage',
        'email',
        'age_group',
        'file_path',
        'school',
        'terms',
        'address',
    ];

    public function getWaterMarkImageAttribute()
    {
        $public_path  = asset('images/placeholder.png');

        if(file_exists(public_path($this->file_path))){
            $file_info   = pathinfo(asset($this->file_path));
            $wm_image = public_path('drawings/'.$this->student_id.'/1.'.$file_info['extension']);
            if (file_exists($wm_image)) {
                return asset('drawings/'.$this->student_id.'/1.'.$file_info['extension']);
            }
        }

        return $public_path;
    }
}
