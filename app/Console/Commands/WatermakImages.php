<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Intervention\Image\Facades\Image;
use App\Models\Student;

class WatermakImages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'watermar-images:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $students = Student::get();

        foreach ($students as $key => $student) {

            if (file_exists(public_path($student->file_path))) {

                $file_info   = pathinfo(public_path($student->file_path));
                $allowed_mie_types = ['jpeg', 'jpg', 'png', 'gif'];

                if ($file_info && in_array($file_info['extension'], $allowed_mie_types) ) {

                    $make_img    = Image::make(public_path($student->file_path));
                    $full_path   = public_path('drawings/'.$student->student_id.'/1.'.$file_info['extension']);

                    if (!file_exists($full_path)) {
                        $make_img->text('Arunalu Siththam 2021', $make_img->width() - 260 , $make_img->height()-30, function($font) {
                            $font->file(public_path('assets/fonts/nunito-semibold-webfont.woff'));
                            $font->size(22);
                            $font->color('#ffff');
                            $font->align('left');
                            $font->valign('top');
                        });
                        $make_img->save($full_path, 90);
                    }

                }

            }
        }

        return "generation completed";

    }
}
