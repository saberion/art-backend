"use strict";

$(function () {
  validation();
  validationForms();
  datePicker();
});

function validation() {
  Dropzone.options.myGreatDropzone = {
    // camelized version of the `id`
    maxFiles: 1,
    paramName: "file",
    // The name that will be used to transfer the file
    maxFilesize: 2,
    // MB
    accept: function accept(file, done) {
      if (file.name == "justinbieber.jpg") {
        done("Naha, you don't.");
      } else {
        done();
      }
    },
    maxfilesexceeded: function maxfilesexceeded(file) {
      this.removeAllFiles();
      this.addFile(file);
    }
  };
}

function validationForms() {
  $("#contactForm").validate({
    rules: {
      fullName: {
        required: true
      },
      dateOfBirth: {
        required: true
      },
      parentName: {
        required: true
      },
      email: {
        required: true,
        email: true
      },
      contactNo: {
        required: true,
        digits: true,
        minlength: 10,
        maxlength: 10
      },
      "check_confirm[]": {
        required: true
      }
    },
    messages: {
      fullName: {
        required: "Please provide your full name"
      },
      dateOfBirth: {
        required: "Please provide date of birth"
      },
      parentName: {
        required: "Please provide parent name"
      },
      contactNo: {
        required: "Please provide phone number"
      },
      email: {
        required: "Please provide your email"
      },
      "check_confirm[]": {
        required: "You must check at least 1 box"
      }
    }
  });
}

function datePicker() {
  $(".data-pick").flatpickr({
    altInput: true,
    altFormat: "F j, Y",
    dateFormat: "Y-m-d"
  });
}

$(document).on('submit','#contactForm', function(e) {
    e.preventDefault();
    e.stopPropagation();
    $('.error').remove();
    $(".dropzone").removeClass('dropzone_error')
    $("#submitArtwork").attr('disabled', true)

    function getValues() {
        var formData = new FormData(document.getElementById("contactForm"));
        formData.append('drawing', $('#DrawingDropzone')[0].dropzone.getAcceptedFiles()[0]);
        return formData;
    }

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        method: 'POST',
        url: '/register',
        data: getValues(),
        processData: false, // required for FormData with jQuery
        contentType: false, // required for FormData with jQuery
        error: function (data) {
            $.each(data.responseJSON.errors, function(input, message) {
                $("#submitArtwork").attr('disabled', false)
                if(input == 'drawing'){
                    $(".dropzone").addClass('dropzone_error')
                }else if(input == 'terms'){
                    $("#"+input).parent('.check-box-wrap').after('<span class="error lbl-error">'+message+'</span>')
                }else{
                    $("#"+input+"").after('<label id="'+input+'-error" class="error" for="'+input+'">'+message+'</label>')
                }
            });
        },
        success: function (response) {
           if(response = 1){
               location.reload()
           }
        }
    });
});
