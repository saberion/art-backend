@if (session()->has('error'))
    @push('scripts')
        <script>
            M.toast({html: '{{ session()->pull('error') }}'});
        </script>
    @endpush
@endif

@if (session()->has('success'))
    @push('scripts')
        <script>
            M.toast({html: '{{ session()->pull('success') }}'});
        </script>
    @endpush
@endif
