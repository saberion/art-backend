@extends('layouts.app')

@section('title', 'Admin Login')

@section('content')
    <div class="main-wrapper">
        <div class="inner-wrapper">
            <div class="top-part">
                <form action="{{ route('admin.login') }}" method="POST">
                    @csrf

                    <div class="contain-inputs">
                        <div class="input-cover left-section">
                            <div class="input-field">
                                <input type="email" name="email" value="{{ old('email') }}" id="email_feild"
                                    class="validate" autocomplete="email" required>
                                <label for="email_feild">Email</label>

                                @error('email')
                                    <span class="helper-text" data-error="{{ $message }}">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="input-field">
                                <input type="password" name="password" id="password" class="validate"
                                    autocomplete="current-password" minlength="8" required>
                                <label for="password">Password</label>

                                @error('password')
                                    <span class="helper-text" data-error="{{ $message }}">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <button type="submit" class="waves-effect waves-light btn">Login</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
