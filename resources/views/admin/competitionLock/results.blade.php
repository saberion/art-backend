@extends('layouts.app')

@section('title', 'Competition Results')

@section('content')
    <div class="main-wrapper">
        @include('admin.auth.logout')

        <div class="spacer jury-sp">
            @include('partials.head', ['title' => 'ADMIN DASHBOARD'])
        </div>

        <div style="width: 100%" class="dashboard jury-dash">
            <div class="table-title">
                @if ($competition == 1)
                    <h2>INTERACTIVE WALL – VITALITY Results</h2>
                @else
                    <h2>BEACH SCULPTURE – DREAM Results</h2>
                @endif
            </div>

            <table class="striped highlight responsive-table">
                <thead>
                    <tr>
                        <th>Account Id</th>
                        <th>Average Points</th>
                        <th>Proposal</th>
                        <th>Design</th>
                        <th>Comments</th>
                    </tr>
                </thead>

                <tbody>
                    @if ($submissions->isNotEmpty())
                        @foreach ($submissions as $submission)
                            <tr>
                                <td class="primary-color">
                                    <a href="{{ route('admin.entries.students', $submission->entry->id) }}">
                                        {{ $submission->entry->identity }}
                                    </a>
                                </td>

                                <td>{{ $submission->result->average_points }}</td>

                                <td class="pdf-view">
                                    <a href="{{ Storage::url($submission->proposal) }}" class="pdf"
                                        target="_blank">
                                        <img src="{{ asset('images/pdf.png') }}" alt="PDF Icon"> View
                                    </a>
                                </td>

                                <td class="pdf-view">
                                    <a href="{{ Storage::url($submission->design) }}" class="pdf"
                                        target="_blank">
                                        <img src="{{ asset('images/pdf.png') }}" alt="PDF Icon"> View
                                    </a>
                                </td>

                                <td>
                                    <a class="modal-trigger" href="#comments{{ $loop->iteration }}">View</a>
                                </td>
                            </tr>

                            @if ($submission->reviews->isNotEmpty())
                                <div id="comments{{ $loop->iteration }}" class="modal">
                                    @foreach ($submission->reviews as $review)
                                        <div class="modal-content">
                                            <p>{{ $review->juryMember->name }} - {{ $review->comments }}</p>
                                        </div>
                                    @endforeach

                                    <div class="modal-footer">
                                        <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $('.modal').modal();
    </script>
@endpush