@extends('layouts.app')

@section('title', 'Competition Lock')

@section('content')
    <div class="main-wrapper">
        @include('admin.auth.logout')

        <div class="spacer jury-sp">
            @include('partials.head', ['title' => 'ADMIN DASHBOARD'])
        </div>

        <div style="width: 100%" class="dashboard jury-dash">
            <div class="table-title">
                <h2>COMPETITION LOCK</h2>
            </div>

            <div class="beach-sculp">
                <div class="rnd-sec">
                    @if ($locks->contains('competition', 1))
                        <div class="lock-icon">
                            <img src="{{ asset('images/lock.png') }}" alt="">
                        </div>
                    @else
                        <div class="lock-icon">
                            <img src="{{ asset('images/round-unlock.png') }}" alt="">
                        </div>
                    @endif

                    <span class="round">INTERACTIVE WALL – VITALITY</span>

                    @if ($locks->contains('competition', 1))
                        <span class="locked">
                            Locked {{ $locks->where('competition', 1)->first()->created_at->diffForHumans() }}
                        </span>

                        <a href="{{ route('admin.competitionLock.results', 1) }}">View Results</a>
                    @else
                        <form action="{{ route('admin.competitionLock.store', 1) }}" method="POST">
                            @csrf

                            <button type="submit" class="waves-effect waves-light btn btn-outline round-lock-btn">
                                LOCK COMPETITION
                            </button>
                        </form>
                    @endif
                </div>

                <div class="rnd-sec">
                    @if ($locks->contains('competition', 2))
                        <div class="lock-icon">
                            <img src="{{ asset('images/lock.png') }}" alt="">
                        </div>
                    @else
                        <div class="lock-icon">
                            <img src="{{ asset('images/round-unlock.png') }}" alt="">
                        </div>
                    @endif

                    <span class="round">BEACH SCULPTURE – DREAM</span>

                    @if ($locks->contains('competition', 2))
                        <span class="locked">
                            Locked {{ $locks->where('competition', 2)->first()->created_at->diffForHumans() }}
                        </span>

                        <a href="{{ route('admin.competitionLock.results', 2) }}">View Results</a>
                    @else
                        <form action="{{ route('admin.competitionLock.store', 2) }}" method="POST">
                            @csrf

                            <button type="submit" class="waves-effect waves-light btn btn-outline round-lock-btn">
                                LOCK COMPETITION
                            </button>
                        </form>
                    @endif
                </div>
            </div>
        </div>
    </div>

    @include('layouts.alerts')
@endsection
