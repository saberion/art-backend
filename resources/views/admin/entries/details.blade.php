@extends('layouts.app')
@section('content')
@push('styles')
<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
<style>
    .btnRotate {
        padding: 5px 10px;
        background-color: #09F;
        border: 0;
        color: #FFF;
        cursor: pointer;
    }
</style>
@endpush
   <div class="container">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <img id="drawing" src="{{asset($student->file_path)}}" alt="" srcset="" class="img-responsive">
            </div>
            <div class="col-md-3"></div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div id="button-container">
                    <p>Rotate Image:</p>
                        <input type="button" class="btnRotate" value="Rotate Image" onClick="rotateImage();" />
                        <input type="button" value="save" onclick="saveRotation()">
                        <input type="button" value="Watermark" onclick="GenerateWatermark({{$student->id}})">
                        <input type="hidden" id="student_id" name="student_id" value="{{$student->id}}">
                        <input type="hidden" id="current_position" name="current_position" value="">
                </div>
                <div class="notifications">
                    <p class="text-dannger">Please hard refresh the browser once edit the image (ctrl + f5)</p>
                </div>
            </div>
        </div>
   </div>
@endsection
@push('scripts')
    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function rotateImage(degree) {

            $('#drawing').animate({  transform: 90 }, {
                step: function(now,fx) {
                    $(this).css({
                        '-webkit-transform':'rotate('+now+'deg)',
                        '-moz-transform':'rotate('+now+'deg)',
                        'transform':'rotate('+now+'deg)'
                    });
                }
            });

            $('#current_position').val(90)

        }

        function saveRotation(){
            $('#button-container').hide()
            $('.notifications').append('<p>Processing...</p>')
            $.ajax({
                method: 'POST',
                url: '/admin/dashboard/rotate-image',
                data:{id:$('#student_id').val(), position:-90},
                error: function (data) {
                alert('Error')
                },
                success: function (response) {
                    $.ajax({
                        url: "",
                        context: document.body,
                        success: function(s,x){

                            $('html[manifest=saveappoffline.appcache]').attr('content', '');
                                $(this).html(s);
                        }
                    });
                    location.reload(true);
                }
            });
        }

        function GenerateWatermark(id){
            $('#button-container').hide()
            $('.notifications').append('<p>Processing...</p>')
            $.ajax({
                method: 'POST',
                url: '/admin/dashboard/watermark-image',
                data:{id:id},
                error: function (data) {
                alert('Error')
                },
                success: function (response) {
                    $.ajax({
                        url: "",
                        context: document.body,
                        success: function(s,x){

                            $('html[manifest=saveappoffline.appcache]').attr('content', '');
                                $(this).html(s);
                        }
                    });
                    location.reload(true);
                }
            });
        }
    </script>
@endpush
