@extends('layouts.app')

@section('title', 'Entry Students')

@section('content')
    <div class="main-wrapper">
        @include('admin.auth.logout')

        <div class="full-contact-container-second">
            @include('partials.head', ['title' => 'STUDENTS'])

            @foreach ($entry->students as $student)
                @include('admin.entries.details', ['entry' => $entry, 'student' => $student, 'edit' => false])
            @endforeach
        </div>
    </div>
@endsection