@extends('layouts.app')

@push('scripts')
    <script src="{{ asset('js/sweetalert2.all.min.js') }}"></script>
@endpush

@section('title', 'Admin Dashboard')
@php
    $age_groups = Config('properties.age_groups');
    $stages = Config('properties.stages');
@endphp
<style>
    .stage-btn {
        width: 129px;
        padding: 10px 0;
        box-shadow: none;
        text-decoration: none;
        color: #fff;
        text-align: center;
        letter-spacing: .5px;
        -webkit-transition: background-color .2s ease-out;
        transition: background-color .2s ease-out;
        cursor: pointer;
        border: none;
        border-radius: 2px;
        display: inline-block;
        vertical-align: middle;
        -webkit-tap-highlight-color: transparent;
    }
    .swal-wide {
         width: auto!important;
    }

    .one-side{
        display: inline-block;
    }

    td, th {
        padding: 9px 0;
    }
    label.trash_filter {
        font-size: 14px;
        cursor: pointer;
    }
    .trash_filter > input {
    opacity: 100%!important;
    pointer-events: all!important;
    left: 12px;
    top: 3px;
    cursor: pointer;
}
    .restore_icon {
        width: 26px;
    }
.top-search form{
    width: 40%;
    margin-left: auto;
    display: flex;
}
.top-search > form > input{
    width: 100%;
    padding-left: 0;
    padding-right: 0;
}
.top-search > form > input:nth-child(1){
    margin-right: 5px;
}
.filter-btn{
    height: 40px !important;
}
.top-search > form > .submit-btn-search{
    width: 200px;
}

.table-top-bar{
    justify-content: flex-end !important;
}
</style>
@section('content')
    <div class="main-wrapper">
        @include('admin.auth.logout')

        <div style="width: 100%" class="dashboard">
            <div class="table-title">
                <h2 style="padding-bottom: 12px;">ENTRANTS</h2>
                <div class="top-search">
                    <form action="{{ route('admin.dashboard.index') }}" method="GET" id="keyword_search">
                        <input class="form-control" type="text" name="keyword"  value="{{request('keyword')}}" placeholder="Name / Student ID / Email">
                        <input class="form-control btn submit-btn-search" type="submit" value="Search">
                    </form>
                </div>

            </div>

            <div class="pagination-sec">
                <div class="record">
                    <span>{{ $students->total() }}</span> <span>records found.</span>
                </div>

                <div class="pagination">
                    @if (request()->filled('competition'))
                        {{ $students->appends(['competition' => request('competition')])->links() }}
                    @else
                        {!! $students->appends(Request::except('page'))->render() !!}
                    @endif
                </div>
            </div>

            <div class="table-top-bar">
                    <form action="{{ route('admin.dashboard.index') }}" method="GET" id="student_filter_form">
                    <div class="one-side">
                        <label for="age_group" style="font-size:14px;">Age group</label>
                      <select name="age_group" id="" class="form-control" style="display: block;width:135px">
                            <option value="" selected>All</option>
                            @foreach ($age_groups as $key => $item)
                                 <option value="{{ $key }}" {{ request('age_group') ==  $key ? 'selected' : '' }}>{{ $item['from'].' - '. $item['to']}}</option>
                            @endforeach
                      </select>
                    </div>

                    <div class="one-side" style="padding-left: 30px">
                        <label for="age_group" style="font-size:14px;">Stage</label>
                        <select name="stage" id="" class="form-control" style="display: block;width:135px">
                            <option value="" selected>All</option>
                            <option value="Initial" {{ request('stage') ==  'Initial' ? 'selected' : '' }}>Initial</option>
                            @foreach ($stages as $key => $item)
                                 <option value="{{ $key }}" {{ request('stage') ==  $key ? 'selected' : '' }}>{{ $item['name']}}</option>
                            @endforeach
                      </select>
                    </div>
                    @if (in_array(auth()->user()->email, config('properties.admins')))
                        <div class="one-side" style="padding-left: 30px;margin-top: 22px;position: relative;">
                            <label for="trash_only" class="trash_filter">
                                <input type="checkbox" name="trash_only" value="1" id="trash_only" {{ Request::get('trash_only') == 1 ? 'checked' : '' }}>
                                Show only trash items
                            </label>
                        </div>
                    @endif
                    <div class="one-side" style="padding-left: 30px;margin-top: 22px;">
                        <button style="width: 100px" type="submit" class="waves-effect waves-light btn filter-btn">Filter</button>
                    </div>
                     <div class="one-side" style="padding-left: 30px;margin-top: 22px;">
                        <button style="width: 100px" type="submit" class="waves-effect waves-light btn filter-btn" name="action" value="export">Export</button>
                    </div>

                    <div class="one-side" style="padding-left: 30px;margin-top: 22px;">
                        <button type="submit" class="waves-effect waves-light btn filter-btn" name="action" value="download_images">Download Images</button>
                    </div>

                    <div class="one-side" style="padding-left: 30px;margin-top: 22px;">
                        <button style="width: 100px" type="submit" class="waves-effect waves-light btn filter-btn" name="reset" value="1">Reset</button>
                    </div>

                </form>
                </div>

            <table class="striped highlight responsive-table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Age Group</th>
                        <th>Created</th>
                        <th>More details</th>
                        <th>Drawing</th>
                        <th>stage</th>

                        @if (Request::has('trash_only'))
                        <th>Restore</th>
                        @else
                        <th>Trash</th>
                        @endif
                    </tr>
                </thead>

                <tbody>
                    @if ($students->isNotEmpty())
                        @php
                            $age_groups =config('properties.age_groups');
                        @endphp
                        @foreach ($students as $student)
                            <tr>
                                <td>{{ $student->student_id }}</td>

                                <td>{{ ucfirst($student->name) }} </td>

                                <td>{{ $age_groups[$student->age_group]['name'] }}</td>

                                <td>{{ $student->created_at->diffForHumans() }}</td>

                                <td><button
                                    data-id="{{ $student->student_id }}"
                                    data-name="{{ $student->name }}"
                                    data-bday="{{ $student->date_of_birth }}"
                                    data-parent="{{ $student->parent_name }}"
                                    data-contact="{{ $student->contact }}"
                                    data-email="{{ $student->email }}"
                                    data-address="{{ $student->address }}"
                                    data-school="{{ $student->school }}"
                                    class="more_details stage-btn" style="background-color: #ab0000">More</button></td>

                                <td>
                                    <button style="background-color: #000000;" class="stage-btn get_drawing" data-id="{{ $student->student_id }}">View drawing</button>
                                    <input type="hidden" name="" class="st_drawing_{{ $student->student_id }}" value="{{ route('admin.dashboard.get_drawing' , $student->student_id) }}">
                                </td>

                                <td>
                                    @php
                                        $current_stage['name'] = 'Initial';
                                        $stages = config('properties.stages');
                                        $current_stage = $student->stage !== null ? $stages[$student->stage] : null;
                                        if($current_stage == null){
                                            $current_stage['name'] = 'Initial stage';
                                            $current_stage['color'] = '#f65f3885';
                                        }
                                        $last_stage = array_key_last($stages);

                                    @endphp
                                    <button
                                    data-status="{{$current_stage['name']}}"
                                    data-next="{{$last_stage !== $student->stage ? $student->stage + 1 : $student->stage}}"
                                    data-id="{{ $last_stage !== $student->stage ? route('admin.dashboard.update_stage', $student->id) : '#' }}"
                                    class="update_stage stage-btn {{ $last_stage == $student->stage ? 'last_stage_message' : ''}}"
                                    style="background-color: {{$current_stage['color']}}" {{$student->trashed() ? 'disabled' : ''}} >{{$current_stage['name']}}</button>
                                </td>

                                <td>
                                    <a class="stage-btn" style="background-color: #004c98;color:#fff" href="{{route('admin.dashboard.edit_drawing', $student->id)}}">Edit</a>
                                </td>

                                <td>
                                    @if ($student->trashed())
                                        <a href="{{ route('admin.dashboard.restore' , $student->id) }}" class="restore-student"><img src="{{ asset('images/restore.png') }}" class="restore_icon"></a>
                                    @else
                                    <form id="deletestudent{{ $student->id }}"  action="{{ route('admin.dashboard.trash' , $student->id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="button" class="trash-student">
                                            <img src="{{ asset('images/delete.png') }}">
                                        </button>
                                    </form>
                                    @endif

                                </td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>

@endsection

@push('scripts')
    <script>
        $('.trash-student').click(function () {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Trash it!'
            }).then((result) => {
                if (result.value) {
                    var deleteFormId = $(this).parent().attr('id');

                    $('#' + deleteFormId).submit();
                }
            })
        });

        $('.restore-student').click(function (event) {
            event.preventDefault();
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, restore it!'
            }).then((result) => {
                if (result.value) {
                    window.location.href = $(this).attr('href');
                }
            })
        });

        $('.get_drawing').click(function () {
            var student_id = $(this).data('id')
            var image_url = $('.st_drawing_'+$(this).data('id')).val()

            Swal.fire({
                imageUrl:image_url,
                text: "Student ID :"+student_id,
                showCloseButton: true,
                showConfirmButton: false,
                focusConfirm: false,
                customClass: 'swal-wide',
            })
        });

        $('.last_stage_message').click(function (event) {
            event.preventDefault();
            Swal.fire({
                title: 'Hey!',
                text: "This competitor has passed all stages",
                icon: 'info',
                confirmButtonColor: '#3085d6',
            })
        });



        $(document).on('submit','#keyword_search', function(e) {
            $(this).find('input[type=text]').each(function(count , element) {
                if($(this).val() == ''){
                    $(this).prop('disabled', true)
                }
            })
        });

    $(document).on('click','.update_stage', function(event) {
        event.preventDefault();
        if($(this).hasClass('last_stage_message')){
            Swal.fire({
                title: 'Hey!',
                text: "The participant has passed all the stages.",
                icon: 'info',
                confirmButtonColor: '#3085d6',
            })
        }else{
            Swal.fire({
                title: 'Are you sure?',
                // text: "You are about to move the submission from "+$(this).data('status')+" to "+$(this).data('next')+". Please confirm to move to the next stage, doing so you will not be able to undo the action.",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'No',
                confirmButtonText: 'Yes',
            html: `<p style="font-size: 14px;line-height: 1.5;">You are about to move the submission from <br>`+$(this).data('status')+` to  stage-`+$(this).data('next')+`.</p> <br>
                <p style="font-size: 14px;line-height: 1.5;"> Please confirm to move to the next stage, doing so you will not be able to undo the action.</p>`
            }).then((result) => {
                if(result['isConfirmed']){
                    window.location.href = $(this).data('id');
                }

            })
        }
    });

     $('.more_details').click(function (event) {
            event.preventDefault();
            Swal.fire({
                title: 'Student ID :' + $(this).data('id'),
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Close',
                showCloseButton: true,
                customClass: 'swal-wide',
                html: `<table id="table" border=1>
                    <tbody>
                    <tr>
                        <th> Participant's Name</th>
                        <td>`+$(this).data('name')+`</td>
                    </tr>
                    <tr>
                        <th>Participant's Date of Birth</th>
                        <td>`+$(this).data('bday')+`</td>
                    </tr>
                    <tr>
                        <th>Postal Address</th>
                        <td>`+$(this).data('address')+`</td>
                    </tr>
                    <tr>
                        <th>School Name</th>
                        <td>`+$(this).data('school')+`</td>
                    </tr>
                    <tr>
                        <th>Parent/Guardian's Name</th>
                        <td>`+$(this).data('parent')+`</td>
                    </tr>
                     <tr>
                        <th>Contact No</th>
                        <td>`+$(this).data('contact')+`</td>
                    </tr>
                    <tr>
                        <th>Email Address</th>
                        <td>`+$(this).data('email')+`</td>
                    </tr>

            </tbody>
            </table>`
            })
        });

        // $('.filter-btn').on('click', function (event) {

        //     $("#student_filter_form").find('input[type=checkbox],select').each(function(count , element) {
        //             if($(this).val() == ''){
        //                 $(this).prop('disabled', true)
        //             }
        //     })

        //     $("#student_filter_form").submit()

        //   })
    </script>
@endpush
