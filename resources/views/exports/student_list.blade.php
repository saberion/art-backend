<table>
    <thead>
    <tr>
        <th style="background-color: #e5e3e1;"><strong>ID</strong> </th>
        <th style="background-color: #e5e3e1;"><strong>Age Group</strong> </th>
        <th style="background-color: #e5e3e1;"><strong>Stage</strong> </th>
        <th style="background-color: #e5e3e1;"><strong>Participant's Name</strong> </th>
        <th style="background-color: #e5e3e1;"><strong>Participant's Date of Birth</strong> </th>
        <th style="background-color: #e5e3e1;"><strong>Postal Address</strong> </th>
        <th style="background-color: #e5e3e1;"><strong>School Name</strong> </th>
        <th style="background-color: #e5e3e1;"><strong>Parent/Guardian's Name</strong> </th>
        <th style="background-color: #e5e3e1;"><strong>Contact No</strong> </th>
        <th style="background-color: #e5e3e1;"><strong>Email Address</strong> </th>
        <th style="background-color: #e5e3e1;"><strong>Created</strong> </th>
    </tr>
    </thead>
    <tbody>
    @foreach($students as $student)
    @php
        $age_group = config('properties.age_groups')[$student->age_group];
        $stages = config('properties.stages');
        $current_stage = $student->stage !== null ? $stages[$student->stage] : null;
        if($current_stage == null){
            $current_stage['name'] = 'Initial';
            $current_stage['color'] = '#ffad98';
        }
        $last_stage = array_key_last($stages);
    @endphp
        <tr>
            <td>{{ $student->student_id }} </td>
            <td>{{ $age_group['from']. '-' .$age_group['to'] }} </td>
            <td style="background-color: {{ $current_stage['color'] }}">{{ $current_stage['name'] }} </td>
            <td>{{ $student->name}} </td>
            <td>{{ $student->date_of_birth }} </td>
            <td>{{ $student->address}} </td>
            <td>{{ $student->school}} </td>
            <td>{{ $student->parent_name}} </td>
            <td>{{ $student->contact}} </td>
            <td>{{ $student->email}} </td>
            <td>{{ $student->created_at}} </td>
        </tr>
    @endforeach
    </tbody>
</table>

