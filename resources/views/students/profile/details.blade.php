@php
    $avatar = ! empty($student->avatar) ? Storage::url($student->avatar)
        : asset('images/profile-image01.png');
@endphp

@if ($edit)
    <form action="{{ route('students.profile.update') }}" method="POST" enctype="multipart/form-data">
@endif

    @csrf
    @method('PATCH')

    <div class="profile-component">
        <div class="profile-card">
            <div class="wrap-back">
                <img src="{{ $avatar }}" alt="{{ $student->name }}'s Profile Picture">

                @if ($edit)
                    <div class="cam-upload">
                        <input type="file" name="students[{{ $student->id }}][avatar]" accept="image/*">
                    </div>
                @endif
            </div>

            <div class="profile-title-section">
                <h5>{{ $student->name }}</h5>
                <h5>{{ $entry->identity }}</h5>
            </div>
        </div>

        <div class="wrap-input-ele profile-spaceing">
            <div class="lft-sec">
                <div class="profile-iteam">
                    <p class="profile-title">NIC</p>
                    <p class="profile-head">{{ $student->nic }}</p>
                </div>

                <div class="profile-iteam">
                    <p class="profile-title">Email Address</p>
                    <p class="profile-head">{{ $student->email }}</p>
                </div>

                <div class="profile-iteam">
                    @if ($edit)
                        <div class="input-field">
                            <input type="tel" name="students[{{ $student->id }}][contact]"
                                value="{{ $student->contact }}" id="contactNo{{ $loop->iteration }}" class="validate"
                                minlength="12" placeholder="+947xzzzzzzz" autocomplete="mobile">
                            <label for="contactNo{{ $loop->iteration }}">Contact Number</label>

                            @error("students.$student->id.contact")
                                <span class="helper-text" data-error="{{ $message }}">{{ $message }}</span>
                            @enderror
                        </div>
                    @else
                        <p class="profile-title">Contact Number</p>
                        <p class="profile-head">{{ $student->contact }}</p>
                    @endif
                </div>
            </div>

            <div class="lft-sec">
                <div class="profile-iteam">
                    <p class="profile-title">University</p>
                    <p class="profile-head">{{ $student->university }}</p>
                </div>

                <div class="profile-iteam">
                    @if ($edit)
                        <div class="input-field">
                            <input type="text" name="students[{{ $student->id }}][registration]"
                                value="{{ $student->registration }}" id="registration{{ $loop->iteration }}"
                                class="validate">
                            <label for="registration{{ $loop->iteration }}">University Student Number</label>

                            @error("students.$student->id.contact")
                                <span class="helper-text" data-error="{{ $message }}">{{ $message }}</span>
                            @enderror
                        </div>
                    @else
                        <p class="profile-title">University Student Number</p>
                        <p class="profile-head">{{ $student->registration }}</p>
                    @endif
                </div>

                <div class="profile-iteam">
                    <div class="profile-iteam">
                        @if ($edit)
                            <div class="input-field">
                                <input type="text" name="students[{{ $student->id }}][course]"
                                    value="{{ $student->course }}" id="course{{ $loop->iteration }}" class="validate">
                                <label for="course{{ $loop->iteration }}">Course Following</label>

                                @error("students.$student->id.course")
                                    <span class="helper-text" data-error="{{ $message }}">{{ $message }}</span>
                                @enderror
                            </div>
                        @else
                            <p class="profile-title">Course Following</p>
                            <p class="profile-head">{{ $student->course }}</p>
                        @endif
                    </div>
                </div>
            </div>

            <div class="lft-sec">
                <div class="profile-iteam">
                    @if ($edit)
                        <div class="input-field">
                            <input type="number" name="students[{{ $student->id }}][year]" value="{{ $student->year }}"
                                id="yearOfStudy{{ $loop->iteration }}" class="validate" min="1" step="1">
                            <label for="yearOfStudy{{ $loop->iteration }}">Year of Study</label>

                            @error("students.$student->id.year")
                                <span class="helper-text" data-error="{{ $message }}">{{ $message }}</span>
                            @enderror
                        </div>
                    @else
                        <p class="profile-title">Year of Study</p>
                        <p class="profile-head">{{ $student->year }}</p>
                    @endif
                </div>

                <div class="profile-iteam">
                    @if ($edit)
                        <button type="submit" class="waves-effect waves-light btn">Save</button>
                    @else
                        <a href="{{ route('students.profile.edit') }}" class="waves-effect waves-light btn">
                            Edit Profile
                        </a>
                    @endif
                </div>
            </div>
        </div>
    </div>

@if ($edit)
    </form>
@endif