@extends('layouts.app')

@section('title', 'Student Profile')

@section('content')
    <div class="main-wrapper">
        @include('students.auth.logout', ['entry' => $entry])

        <div class="full-contact-container-second">
            @include('partials.head', ['title' => 'PROFILE'])

            @foreach ($entry->students as $student)
                @include('students.profile.details', ['entry' => $entry, 'student' => $student, 'edit' => true])
            @endforeach
        </div>
    </div>

    @include('layouts.alerts')
@endsection