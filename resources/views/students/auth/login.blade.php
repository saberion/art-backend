@php
    $entry = ! empty($student)
        ? $student->entries->first()->identity
        : '';

    $username = ! empty($entry) ? $entry : old('username');
@endphp

@extends('layouts.app')

@section('title', 'Student Login')

@section('content')
    <div class="main-wrapper">
        <div class="inner-wrapper">
            <div class="top-part">
                @include('partials.head', [
                    'title_msg' => 'CONGRATULATIONS!',
                    'description_msg' => 'Your group registration is successful and your competitor ID is ',
                    'id_pcc' => $username,
                    'rest_text' => 'Please login here to continue.'
                ])

                <form action="{{ route('students.login') }}" method="POST">
                    @csrf

                    <div class="contain-inputs">
                        <div class="input-cover left-section">
                            <div class="input-field">
                                <input type="text" name="username" value="{{ $username }}" id="Competitior_id"
                                    class="validate" minlength="7" required>
                                <label for="Competitior_id">Competitior ID</label>

                                @error('username')
                                    <span class="helper-text" data-error="{{ $message }}">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="input-field">
                                <input type="password" name="password" id="password" class="validate"
                                    autocomplete="current-password" minlength="8" required>
                                <label for="password">Password</label>

                                @error('password')
                                    <span class="helper-text" data-error="{{ $message }}">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <button type="submit" class="waves-effect waves-light btn">Login</button>
                    </div>

                    <div class="email-sec">
                        <div class="input-field">
                            <input type="email" name="email" value="{{ $student->email ?? old('email') }}"
                                id="email_feild" class="validate" autocomplete="email" required>
                            <label for="email_feild">Email</label>

                            @error('email')
                                <span class="helper-text" data-error="{{ $message }}">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @include('layouts.alerts')
@endsection
