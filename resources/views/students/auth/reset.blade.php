@extends('layouts.app')

@section('title', 'Reset Password')

@section('content')
    <div class="main-wrapper">
        <div class="inner-wrapper">
            <div class="top-part">
                @include('partials.head', [
                    'title' => 'Reset Password',
                    'description' => 'Please use the form below to reset your password.'
                ])

                <form action="{{ route('students.password.update') }}" method="POST">
                    @csrf
                    <input type="hidden" name="id" value="{{ $id }}">
                    
                    <div class="contain-inputs">
                        <div class="input-cover left-section">
                            <div class="input-field">
                                <input type="email" name="email" value="{{ $email ?? old('email') }}" id="email"
                                    class="validate" autocomplete="email" required>
                                <label for="email">Email *</label>

                                @error('email')
                                    <span class="helper-text" data-error="{{ $message }}">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="input-field">
                                <input type="password" name="password" id="password" class="validate"
                                    autocomplete="new-password" minlength="8" required>
                                <label for="password">New Password *</label>

                                @error('password')
                                    <span class="helper-text" data-error="{{ $message }}">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="input-field">
                                <input type="password" name="password_confirmation" id="passwordConfirmation"
                                    class="validate" autocomplete="new-password" minlength="8" required>
                                <label for="passwordConfirmation">Confirm Password *</label>

                                @error('password_confirmation')
                                    <span class="helper-text" data-error="{{ $message }}">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <button type="submit" class="waves-effect waves-light btn">Reset</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @include('layouts.alerts')
@endsection