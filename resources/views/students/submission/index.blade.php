@extends('layouts.app')

@section('title', 'Student Submission')

@section('content')
<div class="main-wrapper">
    @include('students.auth.logout', ['entry' => session('entry')])

    <div class="full-contact-container">
        <div class="spacer">
            @include('partials.head', ['title' => 'COMPETITION'])
        </div>

        <div class="containre-sec">
            <ul class="progressbar">
                <li class="active">
                    <img src="{{ asset('images/tick.png') }}" alt="">
                    Submission
                </li>
                <li>Result</li>
            </ul>
        </div>

        <form action="{{ route('students.submission.store') }}" method="POST" enctype="multipart/form-data">
            @csrf

            <div class="select-competition">
                <span class="title-label">Select competition</span>

                <label>
                    <input type="radio" name="competition" value="1" class="validate" required>
                    <span>Interactive Wall - Submissions Closed</span>
                </label>

                <label>
                    <input type="radio" name="competition" value="2" class="validate" required disabled>
                    <span>Beach Sculpture - Submissions Closed</span>
                </label>
            </div>

            @error('competition')
            <span class="helper-text" data-error="{{ $message }}">{{ $message }}</span>
            @enderror

            <div class="upload-document">
                <span class="title-label">Select competition</span>

                <div class="documents">
                    <div class="doc-one">
                        <p class="box-title">Proposal</p>
                        <p class="format">Format: <span>PDF</span></p>
                        <p class="m-size">Max Size: <span>20MB</span></p>

                        <div class="file-field input-field">
                            <div class="btn">
                                <span>UPLOAD</span>
                                <input type="file" name="proposal" accept=".pdf" class="validate" required>
                            </div>

                            <div class="file-path-wrapper">
                                <input class="file-path validate" type="text">
                            </div>
                        </div>

                        @error('proposal')
                        <span class="helper-text" data-error="{{ $message }}">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="doc-two">
                        <p class="box-title">Design</p>
                        <p class="format">Format: <span>PDF</span></p>
                        <p class="m-size">Max Size: <span>20MB</span></p>

                        <div class="file-field input-field">
                            <div class="btn">
                                <span>UPLOAD</span>
                                <input type="file" name="design" accept=".pdf" class="validate" required>
                            </div>

                            <div class="file-path-wrapper">
                                <input class="file-path validate" type="text">
                            </div>
                        </div>

                        @error('design')
                        <span class="helper-text" data-error="{{ $message }}">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
            </div>

            <p class="agreement">
                <label>
                    <input type="checkbox" name="terms" class="validate" required>
                    <span>I/we have read and understood all the rules & guidelines stipulated under ‘rules &
                        guidelines’ of the competition and agree to the same; and I/We confirm that the design
                        submitted by me/ us is an original work, and it is not copied or reproduced.</span>
                </label>

                {{-- <a href="#modal1" class="modal-trigger tearm">Tearms & Conditions</a> --}}
            </p>

            @error('terms')
            <span class="helper-text" data-error="{{ $message }}">{{ $message }}</span>
            @enderror

            {{-- <div id="modal1" class="modal">
                    <div class="modal-content">
                        <a href="#!" class="close-btn">
                            <img class="modal-close" src="{{ asset('images/close.svg') }}" alt="">
            </a>

            <h4>Terms & Conditions</h4>

            <p>I/we have read and understood all the rules & guidelines stipulated under ‘rules &
                guidelines’ of the competition and agree to the same; and I/We confirm that the design
                submitted by me/ us is an original work, and it is not copied or reproduced.</p>
    </div>
</div> --}}

<div class="submit-button">
    <button data-target="modal12" class="waves-effect waves-light btn modal-trigger">Submit</button>
</div>

<div id="modal12" class="modal">
    <div class="modal-content">
        <h4>Confirmation</h4>

        <p>Hope you have uploaded the required 2 documents against the correct compition.</p>

        <p>By clicking ‘YES’ will complet the sumission and you will not be able to do any amendments
            here after.</p>

        <p>By clicking ‘NO’ you can go back and check on the selections that you have done.</p>
    </div>

    <div class="con-btns">
        <button type="submit" class="waves-effect waves-light btn ">YES</button>
        <button type="button" class="waves-effect waves-light btn btn-outline"
            onclick="$('#modal12').modal('close')">NO</button>
    </div>
</div>
</form>
</div>
</div>

@include('layouts.alerts')
@endsection

@push('scripts')
<script>
    $('.modal').modal();
        $('.modal-confirm').modal();
</script>
@endpush
