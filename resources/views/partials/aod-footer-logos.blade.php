<div class="wrap-com-logos" style="margin-top: 5rem">
    <div class="com-logos">
        <img src="{{ url('/assets') }}/images/logos/logo-1.png" alt="logos" style="max-width: 97px;">
        <img src="{{ url('/assets') }}/images/logos/logo-2.png" alt="logos" style="max-width: 118px;">
        <img src="{{ url('/assets') }}/images/logos/logo-3.png" alt="logos" style="max-width: 130px;">
    </div>
    <img src="{{ url('/assets') }}/images/bottom-icons/fl-bt-1.png" alt="icon" class="fl-bt-1">
    <img src="{{ url('/assets') }}/images/bottom-icons/fl-bt-2.png" alt="icon" class="fl-bt-2">
</div>
