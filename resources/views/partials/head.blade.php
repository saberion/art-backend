<div class="head-title">
    <h1>{{ $title ?? '' }}</h1>

    <p>{{ $description ?? '' }}</p>

    @isset($title_msg, $description_msg, $id_pcc, $rest_text)
        <h1 class="msg-head">{{ $title_msg ?? '' }}</h1>

        <p class="msg-dis">{{ $description_msg ?? '' }} <span class="bold-color">{{ $id_pcc ?? '' }}.</span></p>

        <p>{{ $rest_text ?? '' }}</p>
    @endisset
</div>
