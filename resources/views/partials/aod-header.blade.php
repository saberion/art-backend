<header>
    <div class="container__landing">
       <div class="header">
          <a href="https://www.combank.lk/info/52" class="link">
             <img src="{{ url('/assets') }}/images/left-arrow.svg" alt="icon">
             Back to combank.lk
          </a>
          <img src="{{ url('/assets') }}/images/header/combank-log.svg" alt="logo" class="combank-logo">
       </div>
    </div>
 </header>
 <section class="hero__sec">
    <div class="container__landing">
       <div class="hero__sec__wrap">
            <div class="column">
                <img src="{{ url('/assets') }}/images/arunalu-image-1.png" alt="image" class="left-image">
            </div>
            <div class="column">
                <div class="text_wrap">
                    <h1>Arunalu Siththam {{date("Y")}}</h1>
                    <p>Children’s Art Competition</p>
                </div>
            </div>
            <div class="column">
                <img src="{{ url('/assets') }}/images/arunalu-image-2.png" alt="image" class="right-image">
            </div>
       </div>
       
       <img src="/assets/images/header/arunalu-image-mobile.jpg" alt="banner" class="mobile-banner">
    </div>
 </section>