<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Arunalu</title>
    <link rel="stylesheet" href="{{ url('/assets') }}/css/lib.css">
    <link rel="stylesheet" href="{{ url('/assets') }}/css/winner/winner.css">
    <link rel="stylesheet" href="{{ url('/assets') }}/css/main.css">


</head>
<body class="onload">

   <main>
      @include('partials.aod-header')

      <section class="file-upload-sec">
         <div class="container__landing">
            <div class="wrap-file-upload">
               <div class="dopzone-sec">
                  <h4>Drawing</h4>
                  <div id="dropzone">
                     <form action="#" class="dropzone" id="DrawingDropzone">
                        <div class="wrap-drop">
                           <img src="{{ url('/assets') }}/images/dropzone/dropzone.png" class="file-icon" alt="file-icon">
                           <p class="drop-text"><img src="{{ url('/assets') }}/images/dropzone/clicp.png" alt="icon">
                              <span>Add file</span> or Drop files here</p>
                           <p class="file-size-details">File format: JPG Maximum size: 5MB</p>
                        </div>
                     </form>
                  </div>
               </div>
               <div class="contact-form-section">
                  <h4>Participant's Detail</h4>
                  <form id="contactForm" action="{{ route('register') }}" method="post" enctype="multipart/form-data" >
                     <div class="form-wrap">
                        <div class="sec">
                           <div class="input-wrapper">
                                <input type="text" name="name" id="name" required>
                                <label class="floatLablel" for="name">Participant's Name*</label>
                           </div>
                            <div class="input-wrapper">
                                <input type="text" name="school" id="school" required>
                                <label class="floatLablel" for="school">School Name*</label>
                            </div>
                            <div class="input-wrapper">
                                <input type="text" name="contact" id="contact" required>
                                <label class="floatLablel" for="contact">Contact No</label>
                            </div>
                            <div class="input-wrapper">
                                <input type="text" name="parent_name" id="parent_name" required>
                                <label class="floatLablel" for="parent_name">Parent/Guardian's Name</label>
                            </div>
                        </div>
                        <div class="sec">

                             <div class="input-wrapper">
                                <input type="text" name="date_of_birth" id="date_of_birth" class="data-pick" required>
                                <label class="floatLablel" for="date_of_birth">Participant's Date of Birth*</label>
                                <div class="date-content"></div>
                           </div>

                           <div class="input-wrapper">
                            <input type="text" name="address" id="address" required>
                            <label class="floatLablel" for="address">Postal Address</label>
                        </div>

                        <div class="input-wrapper">
                            <input type="text" name="email" id="email" required>
                            <label class="floatLablel" for="email">Email*</label>
                        </div>

                        </div>
                     </div>
                     <div class="wrp-form-btn">
                        <div class="form-btn-sec">
                            <div class="form-group">
                              <div class="check-box-wrap">
                                 <input type="checkbox" name="terms" id="terms" value="1">
                                 <label for="terms" class="ageement-lbl">I have read and agreed with the
                                     <a href="https://www.combank.lk/info/52" target="_blank">Terms & Conditions</a>.</label>
                              </div>
                            </div>
                            <div class="submit-btn-sec">
                               <button type="submit" class="submit-btn" id="submitArtwork">
                                 Submit Drawing
                                  <img src="{{ url('/assets') }}/images/dropzone/right-arrow.png" alt="">
                               </button>
                            </div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>


            @include('partials.aod-footer-logos')
         </div>
      </section>
   </main>

  <script>
    var siteBaseUrl = '';
  </script>


  <script src="{{ url('/assets') }}/js/lib.js"></script>
  <script src="{{ url('/assets') }}/js/main.js"></script>
  <script src="{{ asset('js/sweetalert2.all.min.js') }}"></script>

    @if ($message = Session::get('error'))
    <script>
        Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Something went wrong! Please try again',
        })
    </script>
    @endif
    @if ($message = Session::get('success'))
        <script>
            Swal.fire({
                title: 'Done!',
                text: "Your drawing has been successfully submitted",
                icon: 'success',
                confirmButtonColor: '#3085d6',
            })

        </script>
    @endif
</body>
</html>
