<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Arunalu</title>
    <link rel="stylesheet" href="{{ url('/assets') }}/css/lib.css">
    <link rel="stylesheet" href="{{ url('/assets') }}/css/winner/winner.css">
    <link rel="stylesheet" href="{{ url('/assets') }}/css/main.css">


</head>

<body class="onload">

    <main>
        @include('partials.aod-header')

        <section>
            <p class="text-center">Time period given for submission of drawings is over. <br> Stay tuned for announcement of winners. </p>
            <div class="container__landing">
                @include('partials.aod-footer-logos')
            </div>
        </section>

    </main>

    <script>
        var siteBaseUrl = '';
    </script>


    <script src="{{ url('/assets') }}/js/lib.js"></script>
    <script src="{{ url('/assets') }}/js/main.js"></script>
    <script src="{{ asset('js/sweetalert2.all.min.js') }}"></script>

    @if ($message = Session::get('error'))
    <script>
        Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Something went wrong! Please try again',
        })
    </script>
    @endif
    @if ($message = Session::get('success'))
    <script>
        Swal.fire({
                title: 'Done!',
                text: "Your drawing has been successfully submitted",
                icon: 'success',
                confirmButtonColor: '#3085d6',
            })

    </script>
    @endif
</body>

</html>