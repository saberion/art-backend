<div class="row main-drawing-container">

    @foreach ($students as $student)
    <div class="col-lg-3 col-md-4 col-sm-12 mb-4 drawing-container">
        <div class="content">
            {{-- <span class="watermark-main">Arunalu Siththam {{date("Y")}}</span> --}}

            <a href="#" class="get_st_drawing" data-id="{{$student->id}}">
                <div class="content-overlay"></div>
                <img class="content-image" src="{{ $student->file_path }}">
                <div class="content-details fadeIn-bottom">
                    <h3 class="content-title">{{$student->name}}</h3>
                    <p class="content-text">{{$student->school}}</p>
                </div>
            </a>
        </div>
    </div>
    @endforeach

    <div class='col-lg-3 col-md-4 col-sm-12 mb-4 d-none loading_list'><div class='content' style='background: #f1f3f5;'><span class='watermark-main'>Arunalu Siththam {{date("Y")}}</span> <a href='#'> <div class='content-overlay'></div> <img class='content-image' src='{{asset('images/Loading_icon.gif')}}'> </a> </div> </div>
    <div class='col-lg-3 col-md-4 col-sm-12 mb-4 d-none loading_list'><div class='content' style='background: #f1f3f5;'><span class='watermark-main'>Arunalu Siththam {{date("Y")}}</span> <a href='#'> <div class='content-overlay'></div> <img class='content-image' src='{{asset('images/Loading_icon.gif')}}'> </a> </div> </div>
    <div class='col-lg-3 col-md-4 col-sm-12 mb-4 d-none loading_list'><div class='content' style='background: #f1f3f5;'><span class='watermark-main'>Arunalu Siththam {{date("Y")}}</span> <a href='#'> <div class='content-overlay'></div> <img class='content-image' src='{{asset('images/Loading_icon.gif')}}'> </a> </div> </div>
    <div class='col-lg-3 col-md-4 col-sm-12 mb-4 d-none loading_list'><div class='content' style='background: #f1f3f5;'><span class='watermark-main'>Arunalu Siththam {{date("Y")}}</span> <a href='#'> <div class='content-overlay'></div> <img class='content-image' src='{{asset('images/Loading_icon.gif')}}'> </a> </div> </div>
</div>
<div class="inline-button">
    <button class="form-controll cta" data-id="12" data-count="{{$student_count}}" id="load_more">Load More</button>
</div>
