@if ($message = Session::get('success'))
<div class="alert alert-success alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>
	<strong>{{ $message }}</strong>
</div>
@endif
@if ($message = Session::get('error'))
<div class="alert alert-danger alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>
	<strong>{{ $message }}</strong>
</div>
@endif
@if ($message = Session::get('submit_success'))
<script>
    Swal.fire({
        title: 'Done!',
        text: "Your drawing has been successfully submitted",
        icon: 'success',
        confirmButtonColor: '#3085d6',
    })

</script>
@endif

<script>
    Swal.fire({
        title: 'Done!',
        text: "Your drawing has been successfully submitted",
        icon: 'success',
        confirmButtonColor: '#3085d6',
    })

</script>
