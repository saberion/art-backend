<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Generate Watermark</title>
</head>
<body>

    <button onclick="InnitiateGenerate('50')">Generate</button>
    <script src="{{asset('assets/js/lib.js')}}"></script>

    <div class="generated">
        <ul class="gn_count">

        </ul>
    </div>

    <script>

        function InnitiateGenerate(amount){
            $('.gn_count').append("<li>start Generating !</li>")
            generate(0, amount)
        }

        function generate(offset, take){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                method: 'POST',
                url: '/image/process',
                data:{offset:offset, take:take},
                error: function (data) {
                alert('Error')
                },
                success: function (response) {
                    loopGeneration(response)
                }
            });
        }

        function loopGeneration(response){
            if (!response.complete) {
                generate(response.offset, response.take)
                $('.gn_count').append("<li> Generated " + response.take+ "</li>")
            }else{
                $('.gn_count').append("<li>Completed !</li>")
            }
        }


    </script>
</body>
</html>
