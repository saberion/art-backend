   <!-- Modal -->

@if (isset($student))
<div class="modal fade" id="DrawingView" tabindex="-1" aria-labelledby="DrawingViewLabel" aria-hidden="true">

    <div class="modal-dialog modal-xl">

        <div class="modal-content">

            {{-- <div class="modal-header">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" onclick="closeButton()"></button>
            </div> --}}

            {{-- <header>
                <div class="container__landing">
                    <div class="header">
                        <a href="{{ route('home') }}" class="link">
                            <img src="{{ url('/assets') }}/images/header/left-arrow.png" alt="icon">
                            Back to Arunalu Sithtam
                        </a>
                        <img src="{{ url('/assets') }}/images/banner/ban-flow-1.png" alt="image"
                            class="fl-5">
                        <img src="{{ url('/assets') }}/images/header/combank-log.png" alt="logo"
                            class="combank-logo">
                    </div>
                </div>
            </header> --}}


            <div class="modal-body">

                <section class="single-winner-sec">
                    <div class="container__landing">

                        <img src="{{ url('/assets') }}/images/banner/ban-flow-3.png" alt="image"
                            class="fl-6">
                        <img src="{{ url('/assets') }}/images/banner/ban-flow-1.png" alt="image"
                            class="fl-7">

                        <div class="col-12 winner-single-content">
                            <div class="card">
                                {{-- <span class="watermark-sub">Arunalu Siththam 2021</span> --}}
                                <div class="img-container">
                                    <img src="{{ $student->file_path }}" alt="{{ $student->name }}">
                                </div>
                            </div>
                        </div>

                        <div class="col-12 winner-single-details">

                            <div class="row">

                                <div class="col-lg-4 col-md-4 col-sm-12 ps-4 winner-name">
                                    <h6 class="ws-title">Name:<h6>
                                            <h5 class="ws-description">{{ $student->name }}<h5>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-12 ps-4 winner-school">
                                    <h6 class="ws-title">School Name:<h6>
                                            <h5 class="ws-description"> {{ $student->school }}<h5>
                                </div>

                                {{-- <div class="col-lg-4 col-md-4 col-sm-12 ps-4 social-share">
                                    <h6 class="ws-title">Share<h6>

                                            <div class="social_media">

                                                <a href="https://www.facebook.com/sharer/sharer.php?u={{ route('student_view', $student->id) }}"
                                                    target="_blank"><i class="fab fa-facebook ps-0"></i></a>
                                                <a href="https://www.instagram.com/?url=https:{{ route('student_view', $student->id) }}"
                                                    target="_blank"><i class="fab fa-instagram"></i></a>
                                                <a href="https://twitter.com/share?url={{ route('student_view', $student->id) }}"
                                                    target="_blank"><i class="fab fa-twitter"></i></a>
                                                <a href="https://wa.me/?text={{ route('student_view', $student->id) }}"
                                                    target="_blank"><i class="fab fa-whatsapp"></i></a>
                                                <a href="http://pinterest.com/pin/create/link/?url={{ route('student_view', $student->id) }}"
                                                    target="_blank"><i class="fab fa-pinterest"></i></a>
                                            </div>

                                </div> --}}

                            </div>
                        </div>

                    </div>
                </section>
            </div>

            {{-- <section class="file-upload-sec">
                <div class="container__landing">

                    @include('partials.aod-footer-logos')
                </div>
            </section> --}}

        </div>
    </div>
</div>
@endif

   <!-- Modal -->
