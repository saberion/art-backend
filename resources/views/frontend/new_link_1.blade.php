<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('assets/css/winner/winner.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/lib/lib.css')}}">

    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css"
        integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />

    <title>Arunalu Siththam {{date("Y")}}</title>

</head>


<body class="onload p-landing">

    <main>
        @include('partials.aod-header')


        <section class="winners-sec">
            <div class="container__landing">
                <div class="winner__sec__wrap">


                    <div class="search-winner">
                        <div class="row search-row">

                            <div class="col-lg-3 col-md-3 col-sm-12 d-flex justify-content-center entries-txt">
                                <p class="m-0">We had over 13000 entries<p>
                            </div>

                            <form action="{{route('home')}}" method="get" id="student_filter_form" class="col-lg-9 col-md-9 col-sm-12">

                                <div class="row justify-content-end">

                                    <div class="col-lg-8 col-md-7 col-sm-12 icon-input">

                                        <input class="icon-input__text-field" id="keyword_field" type="text"
                                            placeholder="Search Student Name / Contact No / Email" name="find"
                                            value="{{Request('find')}}">
                                        <button id="submit_form" type="button" class="btn btn-link text-primary" >
                                            <i class="fa fa-search"></i></button>

                                    </div>

                                    <div
                                        class="col-lg-4 col-md-5 col-sm-12 d-flex justify-content-end dropdown winner-category">
                                        <span class="age-group mr-4">Age Group:</span>

                                        <select class="category-age" name="age_group" id="age_filter">
                                            <option value="">All</option>
                                            @foreach ($age_groups as $key => $item)
                                            <option value="{{$key}}"
                                                {{Request('age_group')  == $key ? 'selected' : '' }}>
                                                {{$item['name']}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>

                            </form>

                        </div>
                    </div>


                    <div class="col-12 winners_img_grid">
                        @include('frontend.winner_list')
                    </div>


                </div>
            </div>
        </section>


        <section class="file-upload-sec">
            <div class="container__landing">

                @include('partials.aod-footer-logos')
            </div>
        </section>

        <div id="drawingModel">@include('frontend.model', ['student' => null])</div>
    </main>

    <script src="{{asset('assets/js/lib.js')}}"></script>

       <script>


        $('body').on('change', '#age_filter', function () {
            // hideEmpty()
            $('#student_filter_form').submit()
        })


        function hideEmpty() {
            $("form#student_filter_form :input").each(function () {
                if ($(this).val() == '') {
                    $(this).attr('disabled', true)
                }
            });
        }

        $('body').on('click', '#submit_form', function (event){
            if ($("#keyword_field").val() !== '') {
                hideEmpty()
                $('#student_filter_form').submit()

            }else{
                event.preventDefault()
            }
        })

        $('body').on('click', '.get_st_drawing', function (event){
            event.preventDefault()

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                method: 'GET',
                url: '/student/'+$(this).data('id'),
                processData: false, // required for FormData with jQuery
                contentType: false, // required for FormData with jQuery
                error: function (data) {
                   alert('Error')
                },
                success: function (response) {
                    if(response.success = true){
                            $("#drawingModel").html(response.view)
                            $("#DrawingView").modal('show')
                    }
                }
            });
        })



        $(document).on('click','#load_more', function (event) {

            var offset = $('.drawing-container').length;
            var total  = $(this).data('count')
            if (offset <= total) {
                $('.loading_list').removeClass('d-none')
                var take   = 12;
                loadMoreData(offset, take);
            }else{
                alert('No more data')
            }
        })



        function loadMoreData(offset, take) {
            if(window.location.href.match(/\?./)) {
                var url = document.location.href+"&offset="+offset+"&take="+take;
            }else{
                var url = document.location.href+"?offset="+offset+"&take="+take;
            }

            $.ajax({
                method: 'GET',
                url: url,
                processData: false, // required for FormData with jQuery
                contentType: false, // required for FormData with jQuery
                error: function (data) {
                   alert('Error')
                },
                success: function (response) {
                    $('.loading_list').addClass('d-none')
                    jQuery.each(response.data, function(index, item) {
                            $('.main-drawing-container').append('<div class="col-lg-3 col-md-4 col-sm-12 mb-4 drawing-container"> <div class="content"> <span class="watermark-main">Arunalu Siththam 2021</span> <a href="#" class="get_st_drawing" data-id="'+item.id+'"> <div class="content-overlay"></div> <img class="content-image" src="'+item.file_path+'"> <div class="content-details fadeIn-bottom"> <h3 class="content-title">'+item.name+'</h3> <p class="content-text">'+item.school+'</p> </div> </a> </div> </div>')
                    });

                }
            });

        }


    </script>

    <script>
        document.addEventListener("contextmenu", function (e) {
            e.preventDefault();
        }, false);
    </script>

<!-- Custom modal close button -->
    <script>
        function closeButton() {
            document.getElementById("DrawingView").style.display = "none";
            document.getElementsByClassName("modal-backdrop").style.display = "none";
        }
    </script>
 <!-- Custom modal close button -->

</body>

</html>
