<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('assets/css/winner/winner.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/lib/lib.css')}}">

    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css"
        integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />

    <title>Arunalu Siththam {{date("Y")}}</title>

</head>
<body class="onload p-landing">
    <main>
        <header>
            <div class="container__landing">
                <div class="header">
                    <a href="{{route('home')}}" class="link">
                        <img src="{{ url('/assets') }}/images/header/left-arrow.png" alt="icon">
                        Back
                    </a>
                    <img src="{{ url('/assets') }}/images/banner/ban-flow-1.png" alt="image" class="fl-5">
                    <img src="{{ url('/assets') }}/images/header/combank-log.png" alt="logo" class="combank-logo">
                </div>
            </div>
        </header>

        <section class="single-winner-sec">
            <div class="container__landing">

                <img src="{{ url('/assets') }}/images/banner/ban-flow-3.png" alt="image" class="fl-6">
                <img src="{{ url('/assets') }}/images/banner/ban-flow-1.png" alt="image" class="fl-7">

                <div class="col-12 winner-single-content">
                    <div class="card">
                         {{-- <span class="watermark-sub">Arunalu Siththam 2021</span> --}}
                        <div class="img-container">
                            <img src="{{ $student->water_mark_image}}" alt="{{$student->name}}">
                        </div>
                    </div>
                </div>

                <div class="col-12 winner-single-details">

                    <div class="row">

                        <div class="col-lg-4 col-md-4 col-sm-12 ps-4 winner-name">
                            <h6 class="ws-title">Name:<h6>
                                    <h5 class="ws-description">{{$student->name}}<h5>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-12 ps-4 winner-school">
                            <h6 class="ws-title">School Name:<h6>
                                    <h5 class="ws-description">{{$student->school}}<h5>

                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-12 ps-4 social-share">
                            <h6 class="ws-title">Share<h6>

                                    <div class="social_media">

                                        <a href="https://www.facebook.com/sharer/sharer.php?u={{ route('student_view', $student->id) }}"  target="_blank"><i class="fab fa-facebook ps-0"></i></a>
                                        <a href="https://www.instagram.com/?url=https:{{ route('student_view', $student->id) }}"  target="_blank"><i class="fab fa-instagram"></i></a>
                                        <a href="https://twitter.com/share?url={{ route('student_view', $student->id) }}"  target="_blank"><i class="fab fa-twitter"></i></a>
                                        <a href="https://wa.me/?text={{ route('student_view', $student->id) }}"  target="_blank"><i class="fab fa-whatsapp"></i></a>
                                        <a href="http://pinterest.com/pin/create/link/?url={{ route('student_view', $student->id) }}"  target="_blank"><i class="fab fa-pinterest"></i></a>
                                    </div>

                        </div>

                    </div>
                </div>

            </div>
        </section>

        <section class="file-upload-sec">
            <div class="container__landing">

                @include('partials.aod-footer-logos')
            </div>
        </section>
    </main>

    <script src="{{asset('assets/js/lib.js')}}"></script>
    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
    </script>

<script>
    document.addEventListener("contextmenu", function (e) {
        e.preventDefault();
    }, false);
</script>

</body>

</html>
