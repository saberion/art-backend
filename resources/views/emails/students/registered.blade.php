<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Arunalu</title>
</head>
<body>
    <h3>Dear {{ $student->parent_name }}</h3>
<p>Thank you for entering your Child's drawing to the <b>Arunalu Siththam {{date("Y")}}</b> art competition organised by the Commercial Bank of Ceylon PLC. </p>

<p>We acknowledge your entry with below details and your reference for the competition is <b>{{ $student->student_id }}</b></p>
<br>
<table>
<tr>
    <td>Participant's Name</td>
    <td> -</td>
    <td>{{ $student->name }}</td>
</tr>
<tr>
    <td>Participant's Date of Birth</td>
    <td> -</td>
    <td>{{ $student->date_of_birth }}</td>
</tr>
<tr>
    <td>Postal Address</td>
    <td> -</td>
    <td>{{ $student->address }}</td>
</tr>
<tr>
    <td>School Name </td>
    <td> -</td>
    <td>{{ $student->school }}</td>
</tr>
<tr>
    <td>Parent/Guardian's Name </td>
    <td> -</td>
    <td>{{ $student->parent_name }}</td>
</tr>
<tr>
    <td>Contact No</td>
    <td> -</td>
    <td>{{ $student->contact }}</td>
</tr>
<tr>
    <td>Email Address</td>
    <td> -</td>
    <td>{{ $student->email }}</td>
</tr>

</table>
<p>Please retain the drawing with you as you will be requested to submit the drawing to the Bank if the drawing is selected as a winning entry. You will be informed by the Bank if your child emerges as a winner.</p>

<p>Wish you all the best.</p>
<p>Organizing Team</p>
<p>Arunalu Siththam {{date("Y")}}</p>

</body>
</html>
