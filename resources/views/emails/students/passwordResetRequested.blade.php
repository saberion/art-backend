<p>Dear Entrant,</p>

<p>You recently requested to reset the password for your PCC – Student Design Competition website back-end portal. Please click the link below to reset it.</p>

<a href="{{ $url }}">Reset Password</a>

<p>If you didn’t request to reset the password, please ignore this email. This password reset is only valid for the next 30 minutes ONLY.</p>

<p>Thanks,</p>

<p>Team - Student Design Competition</p>