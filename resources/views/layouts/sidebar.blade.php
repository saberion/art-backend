@php
    $route = Route::currentRouteName();
@endphp

<section id="sidebar">
    <div class="logo-sec">
        <img src="{{ asset('assets/images/header/combank-log.png') }}" alt="" style="width: 114px;">

        <span class="text-uppercase">Arunalu Siththam {{date("Y")}}</span>
    </div>

    @auth('admin')
        <div class="page-link">
            <ul>
                <li><a class="{{ Str::contains($route, 'dashboard') ? 'active' : '' }}" href="{{ route('admin.dashboard.index') }}">COMPETITORS </a></li>
            </ul>
        </div>
    @endauth

    <div class="portlogo">
        <img src="{{ asset('assets/images/banner/caract-1.png') }}" alt="">
    </div>
</section>
