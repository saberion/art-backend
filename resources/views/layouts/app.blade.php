<!doctype html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta http-equiv='cache-control' content='no-cache'>
    <meta http-equiv='expires' content='0'>
    <meta http-equiv='pragma' content='no-cache'>

    <title>Arunalu Siththam - {{date('Y')}} | @yield('title')</title>

    <link rel="stylesheet" href="{{  asset('css/materialize.min.css') }}">
    <link rel="stylesheet" href="{{  asset('css/app.css') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
    @stack('styles')
</head>

<body>
    <div class="main-container">
        @include('layouts.sidebar')

        <main id="main-section" role="main">
            @include('admin.includes.alerts')
            @yield('content')

            @include('layouts.footer')
        </main>
    </div>

    {{-- <script src="{{ asset('js/jquery.slim.min.js') }}"></script> --}}
    <script src="{{ asset('js/materialize.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery-3.6.0.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>

    @stack('scripts')
</body>

</html>
