@extends('layouts.app')

@section('title', 'Jury Member Dashboard')

@section('content')
    <div class="main-wrapper">
        @include('juryMembers.auth.logout')

        <div class="spacer">
            <div class="head-title" style="padding-bottom: 12px;">
                <h1>JURY DASHBOARD</h1>
            </div>
        </div>
        <div class="full-contact-container">
            <div style="width: 100%" class="dashboard">
                <div class="table-title">
                    @if ($competition === "1")
                        <h2 style="padding-bottom: 12px;">INTERACTIVE WALL</h2>
                    @else
                        <h2 style="padding-bottom: 12px;">BEACH SCULPTURE</h2>
                    @endif
                </div>

                <div class="pagination-sec">
                    <div class="record">
                        <span>{{ $submissions->total() }}</span> <span>records found.</span>
                    </div>

                    <div class="pagination">
                        {{ $submissions->appends(['competition' => request('competition')])->links() }}
                    </div>
                </div>

                <table class="striped highlight responsive-table">
                    <thead>
                        <tr>
                            <th>Account ID</th>
                            <th>Proposal</th>
                            <th>Design</th>
                            <th>Points</th>
                            <th>Comments</th>
                        </tr>
                    </thead>

                    <tbody>
                        @if ($submissions->isNotEmpty())
                            @foreach ($submissions as $submission)
                                <tr>
                                    <td class="primary-color">{{ $submission->entry->identity }}</td>

                                    <td class="pdf-view">
                                        <a href="{{ Storage::url($submission->proposal) }}" class="pdf"
                                            target="_blank">
                                            <img src="{{ asset('images/pdf.png') }}" alt="PDF Icon"> View
                                        </a>
                                    </td>

                                    <td class="pdf-view">
                                        <a href="{{ Storage::url($submission->design) }}" class="pdf" target="_blank">
                                            <img src="{{ asset('images/pdf.png') }}" alt="PDF Icon"> View
                                        </a>
                                    </td>

                                    @if ($submission->review)
                                        <td>{{ $submission->review->points }}</td>
                                    @else
                                        <td>Pending</td>
                                    @endif

                                    @if ($submission->review)
                                        <td>
                                            <a class="modal-trigger" href="#comment{{ $loop->iteration }}">View</a>
                                        </td>
                                    @else
                                        <td>Pending</td>
                                    @endif

                                    @if (! $submission->review)
                                        <td>
                                            <a href="{{ route('juryMembers.review.create', ['id' => $submission->id, 'page' => request('page', 1)]) }}"
                                                class="waves-effect waves-light btn">Review</a>
                                        </td>
                                    @endif
                                </tr>

                                @if ($submission->review)
                                    <div id="comment{{ $loop->iteration }}" class="modal">
                                        <div class="modal-content">
                                            <p>{{ $submission->review->comments }}</p>
                                        </div>

                                        <div class="modal-footer">
                                            <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    @include('layouts.alerts')
@endsection

@push('scripts')
    <script>
        $('.modal').modal();
    </script>
@endpush