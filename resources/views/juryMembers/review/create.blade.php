@extends('layouts.app')

@section('title', 'Jury Member Review')

@section('content')
    <div class="main-wrapper">
        @include('juryMembers.auth.logout')

        <div class="full-contact-container">
            <div class="spacer">
                @include('partials.back', ['url' => url()->previous()])

                <div class="head-title" style="padding-bottom: 12px;">
                    <h1>JURY DASHBOARD</h1>
                </div>
            </div>

            <div style="width: 100%" class="dashboard">
                <div class="table-title">
                    <h2 style="padding-bottom: 6px;">
                        ADD YOUR REVIEW FOR {{ $submission->entry->identity }}
                    </h2>
                </div>
            </div>

            <form action="{{ route('juryMembers.review.create', ['id' => $id, 'page' => request('page', 1)]) }}"
                method="POST">
                @csrf

                <div class="form-wrapper">
                    <h4>Points</h4>

                    <div class="input-field">
                        <input type="number" name="points" id="points" class="validate" min="1" max="10" step="1" required>

                        <label for="points">Please mark the points out of 10</label>

                        @error('points')
                            <span class="helper-text" data-error="{{ $message }}">{{ $message }}</span>
                        @enderror
                    </div>
                </div>

                <div class="form-wrapper">
                    <h4>Comments</h4>

                    <div class="input-field">
                        <textarea name="comments" id="comments" class="materialize-textarea validate" required></textarea>

                        <label for="comments">Please provide your comments regarding the design and proposel.</label>

                        @error('comments')
                            <span class="helper-text" data-error="{{ $message }}">{{ $message }}</span>
                        @enderror
                    </div>
                </div>

                <div class="form-wrapper">
                    <button data-target="modal12" type="submit"
                        class="waves-effect save waves-light btn modal-trigger">SAVE</button>
                </div>

                <div id="modal12" class="modal">
                    <div class="modal-content">
                        <h4>Confirmation</h4>

                        <p>Dear Jury member, you're about to grant points and comment on the submitted design.</p>

                        <p>Clicking ‘YES’ will complete the submission and you will not be able to do any amendments
                            here after.</p>

                        <p>Click YES to confirm and save the given points and comment.</p>
                        <p>Click NO to discard.</p>
                    </div>

                    <div class="con-btns">
                        <button type="submit" class="waves-effect waves-light btn ">YES</button>
                        <button type="button" class="waves-effect waves-light btn btn-outline"
                            onclick="$('#modal12').modal('close')">NO</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    @include('layouts.alerts')
@endsection

@push('scripts')
    <script>
        $('.modal').modal();
        $('.modal-confirm').modal();
    </script>
@endpush