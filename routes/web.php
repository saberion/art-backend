<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontEnd\HomeController@index')->name('home');
Route::get('student/{id}', 'FrontEnd\HomeController@view')->name('student_view');
// Route::get('image/{file_path}', 'FrontEnd\HomeController@getPubliclyStorgeFile')->name('show_image');
Route::post('image/process', 'FrontEnd\HomeController@generateWatermarkImage');
Route::get('image/process', 'FrontEnd\HomeController@showGenerator');
Route::post('register', 'FrontEnd\StudentController@store')->name('register');

Route::group([
    'as'            => 'admin.',
    'prefix'        => 'admin',
    'namespace'     => 'Admin',
    'middleware'    => 'guest'
], function () {
    Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('login', 'LoginController@login');
});

Route::group([
    'as'            => 'admin.dashboard.',
    'prefix'        => 'admin/dashboard',
    'namespace'     => 'Admin',
    'middleware'    => 'auth:admin'
], function () {
    Route::get('', 'DashboardController@index')->name('index');
    Route::delete('trash/{id}', 'DashboardController@Trash')->name('trash');
    Route::get('restore/{id}', 'DashboardController@Restore')->name('restore');
    Route::get('export', 'DashboardController@export')->name('export');
    Route::get('getdrwawing/{st_id}', 'DashboardController@Drawing')->name('get_drawing');
    Route::get('update_stage/{st_id}', 'DashboardController@UpdateStage')->name('update_stage');
    Route::get('resubmit', 'DashboardController@resubmit');
    Route::get('edit/{id}', 'DashboardController@edit')->name('edit_drawing');
    Route::post('rotate-image', 'DashboardController@RotateImage');
    Route::post('watermark-image', 'DashboardController@WatermarkImage');
});



Route::post('admin/logout', 'Admin\LoginController@logout') ->name('admin.logout');
