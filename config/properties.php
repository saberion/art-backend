<?php

return [

    'age_groups' => [
        '1' => ['from' => 3,'to' => 5, 'name' => '4 - 5'],
        '2' => ['from' => 6,'to' => 7, 'name' => '6 - 7'],
        '3' => ['from' => 8,'to' => 10, 'name' => '8 - 10'],
        '4' => ['from' => 11,'to' => 13, 'name' => '11 - 13'],
        '5' => ['from' => 14,'to' => 16, 'name' => '14 - 16'],
    ],

    'stages' => [
        '1' => ['name' => 'stage-1' , 'color' => '#fbe7ad'],
        '2' => ['name' => 'Stage-2', 'color' => '#ffc3e1'],
        '3' => ['name' => 'stage-3', 'color' => '#4caf50'],
    ],

    // These are temporary till the jury member accounts are implemented
    'admins' => [
        'admin@saberion.com',
        'sajeevant@arunalusiththam.lk',
        'gayan_piumal@arunalusiththam.lk'
    ]
];
