const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel publiclication. By default, we are compiling the Sass
 | file for the publiclication as well as bundling up all the JS files.
 |
 */


mix.copyDirectory('resources/css/lib/', 'public/assets/lib')
mix.combine([
    'public/assets/lib/css/bootstrap.min.css',
    'public/assets/lib/css/regular.min.css',
    'public/assets/lib/css/font-awesome.min.css',
    'public/assets/lib/css/dropzone.css',
    'public/assets/lib/css/flatpickr.min.css',
], 'public/assets/lib/css/lib.css')

mix.combine([
    'public/assets/js/jquery-3.6.0.min.js',
    'js/jquery.slim.min.js',
    'public/assets/js/jquery.validate.js',
    'public/assets/js/bootstrap.bundle.min.js',
    'public/assets/js/dropzone.js',
    'public/assets/js/flatpickr.js',
], 'public/assets/js/lib.js')

mix.sass('resources/sass/winners/winner.scss', 'public/assets/css/winner')

mix.sass('public/assets/scss/main.scss', 'public/css'),
mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .sass('resources/sass/competition/competitions.scss', 'public/css')
    .copyDirectory('resources/images', 'public/images');

